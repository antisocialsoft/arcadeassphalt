using System;
using System.Collections.Generic;
using System.Diagnostics;
using Godot;
using ProjectArcadeAssphalt.Addons.Widgets;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Resources;

namespace ProjectArcadeAssphalt.Addons.PortraitImporter
{
	[Tool]
	public class PortraitGeneratorUI : PanelContainer
	{
		protected Dictionary<PortraitConstant, HashSet<PortraitResourceWrapper>> PortraitLookup { get; } = new Dictionary<PortraitConstant, HashSet<PortraitResourceWrapper>>();

		protected Dictionary<string, PortraitConstant> FilenamePortraitLookup { get; } = new Dictionary<string, PortraitConstant>
		{
			{"background", PortraitConstant.Background},
			{"body", PortraitConstant.Body},
			{"bodyback", PortraitConstant.BodyBack},
			{"earl", PortraitConstant.Ear_L},
			{"earr", PortraitConstant.Ear_R},
			{"badge", PortraitConstant.EnforcerBadge},
			{"eyel", PortraitConstant.Eye_L},
			{"eyer", PortraitConstant.Eye_R},
			{"logo", PortraitConstant.GangIcon},
			{"hair", PortraitConstant.Hair},
			{"head", PortraitConstant.Head},
			{"mouth", PortraitConstant.Mouth},
			{"nose", PortraitConstant.Nose},
			{"accessory", PortraitConstant.Accessory},
			{"hairback", PortraitConstant.HairBack},
			{"hat", PortraitConstant.Hat},
		};

		protected Dictionary<PortraitConstant, string> PortraitTemplateLookup { get; } = new Dictionary<PortraitConstant, string>
		{
			{PortraitConstant.Background, "res://Resources/PortraitTemplates/Background_Template.tres"},
			{PortraitConstant.Body, "res://Resources/PortraitTemplates/Body_Template.tres"},
			{PortraitConstant.BodyBack, "res://Resources/PortraitTemplates/BodyBack_Template.tres"},
			{PortraitConstant.Ear_L, "res://Resources/PortraitTemplates/EarLeft_Template.tres"},
			{PortraitConstant.Ear_R, "res://Resources/PortraitTemplates/EarRight_Template.tres"},
			{PortraitConstant.EnforcerBadge, "res://Resources/PortraitTemplates/Badge_Template.tres"},
			{PortraitConstant.Eye_L, "res://Resources/PortraitTemplates/EyeLeft_Template.tres"},
			{PortraitConstant.Eye_R, "res://Resources/PortraitTemplates/EyeRight_Template.tres"},
			{PortraitConstant.GangIcon, "res://Resources/PortraitTemplates/Logo_Template.tres"},
			{PortraitConstant.Hair, "res://Resources/PortraitTemplates/Hair_Template.tres"},
			{PortraitConstant.Head, "res://Resources/PortraitTemplates/Head_Template.tres"},
			{PortraitConstant.Mouth, "res://Resources/PortraitTemplates/Mouth_Template.tres"},
			{PortraitConstant.Nose, "res://Resources/PortraitTemplates/Nose_Template.tres"},
			{PortraitConstant.Accessory, "res://Resources/PortraitTemplates/Accessory_Template.tres"},
			{PortraitConstant.HairBack, "res://Resources/PortraitTemplates/HairBack_Template.tres"},
			{PortraitConstant.Hat, "res://Resources/PortraitTemplates/Hat_Template.tres"},
		};

		protected PortraitPartSetWrapper PortraitParts { get; set; } = null;

		public override void _EnterTree()
		{
			//LoadPortraitParts("res://Resources/PortraitParts/");

			// PackedScene partInspectorScene = GD.Load<PackedScene>("res://addons/Widgets/PartInspector.tscn");
			// foreach(HashSet<PortraitResourceWrapper> resourceList in PortraitLookup.Values)
			// {
			// 	foreach(PortraitResourceWrapper part in resourceList)
			// 	{
			// 		PartInspector partInspector = partInspectorScene.Instance() as PartInspector;
			// 		GetNode("HBoxContainer/ScrollContainer/VBoxContainer").AddChild(partInspector);
			// 		partInspector.Part = part;
			// 	}
			// }
			PortraitParts = new PortraitPartSetWrapper(GD.Load<Godot.Object>("res://Resources/PortraitSet.tres"));
		}

		public override void _Ready()
		{
			RefreshPartList();
		}

		public void LoadPortraitParts(string directory)
		{
			Directory dir = new Godot.Directory();
			if (dir.Open(directory) == Error.Ok)
			{
				dir.ListDirBegin(true);
				string fileName = dir.GetNext();
				while (!fileName.Empty())
				{
					if (dir.CurrentIsDir())
					{
						LoadPortraitParts(directory + "/" + fileName);
					}
					else
					{
						// Anything else isn't a portrait resource
						if (fileName.EndsWith(".tres"))
						{
							Resource resource = GD.Load(directory + "/" + fileName);
							PortraitResourceWrapper part = new PortraitResourceWrapper(resource);
							//if(resource is PortraitResourceWrapper part)
							//{
							if (!PortraitLookup.ContainsKey(part.PartFlag))
							{
								PortraitLookup[part.PartFlag] = new HashSet<PortraitResourceWrapper>();
							}

							PortraitLookup[part.PartFlag].Add(part);
							//}
						}
					}
					fileName = dir.GetNext();
				}
			}
			else
			{
				Debug.Fail("Could not open portrait directory: " + directory);
			}
		}

		public void OnImportNewPressed()
		{
			FileDialog fileDialog = new FileDialog();
			AddChild(fileDialog);
			fileDialog.Mode = Godot.FileDialog.ModeEnum.OpenDir;
			fileDialog.PopupExclusive = true;
			fileDialog.PopupCentered(new Vector2(480, 320));
			fileDialog.Connect("dir_selected", this, nameof(OnImportDialogConfirmed));
		}

		private void OnImportDialogConfirmed(string directory)
		{
			ImportPortraitParts(directory);

			RefreshPartList();
		}

		private void RefreshPartList()
		{
			Node vBox = GetNode("HBoxContainer/ScrollContainer/VBoxContainer");
			while (vBox.GetChildCount() > 0)
			{
				Node child = vBox.GetChild(0);
				vBox.RemoveChild(child);
				child.QueueFree();
			}
			PackedScene partInspectorScene = GD.Load<PackedScene>("res://addons/Widgets/PartInspector.tscn");
			VBoxContainer partList = GetNode<VBoxContainer>("HBoxContainer/ScrollContainer/VBoxContainer");
			foreach (Godot.Object unwrapped in PortraitParts)
			{
				PortraitResourceWrapper part = new PortraitResourceWrapper(unwrapped);
				PartInspector partInspector = partInspectorScene.Instance() as PartInspector;
				partList.AddChild(partInspector);
				partInspector.PartSet = PortraitParts;
				partInspector.Part = part;
			}
		}

		public void ImportPortraitParts(string directory)
		{
			Directory dir = new Godot.Directory();
			if (dir.Open(directory) == Error.Ok)
			{
				dir.ListDirBegin(true);
				string fileName = dir.GetNext();
				while (!fileName.Empty())
				{
					if (dir.CurrentIsDir())
					{
						LoadPortraitParts(directory + "/" + fileName);
					}
					else
					{
						// Consider all images new portrait parts
						if (fileName.EndsWith(".png"))
						{

							AddPortraitImageToParts(directory + "/" + fileName);
						}
					}
					fileName = dir.GetNext();
				}
			}
			else
			{
				Debug.Fail("Could not open portrait directory: " + directory);
			}
		}

		private void AddPortraitImageToParts(string filename)
		{
			Texture partTexture = GD.Load<Texture>(filename);
			string partName = System.IO.Path.GetFileNameWithoutExtension(filename);

			if (PortraitParts.IsDuplicate(partName))
			{
				// Easy out of it's a duplicate
				return;
			}

			string[] split = partName.Split("_");
			string typeName = split[split.Length - 1];
			PortraitConstant pFlag = PortraitConstant.Background;

			if (FilenamePortraitLookup.ContainsKey(typeName))
			{
				pFlag = FilenamePortraitLookup[typeName];
			}
			else
			{
				Debug.Fail("Unknown part type: " + typeName);
			}

			Debug.Assert(PortraitTemplateLookup.ContainsKey(pFlag));
			PortraitResourceWrapper template = new PortraitResourceWrapper(GD.Load<Resource>(PortraitTemplateLookup[pFlag]).Duplicate());
			//SavePart(template, partName);

			PortraitResourceWrapper newResource = template;//GD.Load<PortraitResourceWrapper>("res://Resources/PortraitParts/" + partName + ".tres");
			newResource.ResourceName = partName;
			newResource.PartTexture = partTexture;

			switch (pFlag)
			{
				case PortraitConstant.Ear_L:
				case PortraitConstant.Ear_R:
				case PortraitConstant.Eye_L:
				case PortraitConstant.Eye_R:
				case PortraitConstant.BodyBack:
				case PortraitConstant.Body:
				case PortraitConstant.HairBack:
				case PortraitConstant.Hair:
					{
						FindAndAssignOppositePart(newResource);
					}
					break;
			}

			SavePart(newResource, newResource.ResourceName);

			PortraitParts.AddPart(newResource);

		}

		private void FindAndAssignOppositePart(PortraitResourceWrapper part)
		{
			foreach (Godot.Object unwrapped in PortraitParts)
			{
				PortraitResourceWrapper opPart = new PortraitResourceWrapper(unwrapped);
				bool oppositePart = (part.PartFlag == PortraitConstant.Ear_L && opPart.PartFlag == PortraitConstant.Ear_R) ||
									(part.PartFlag == PortraitConstant.Ear_R && opPart.PartFlag == PortraitConstant.Ear_L) ||
									(part.PartFlag == PortraitConstant.Eye_L && opPart.PartFlag == PortraitConstant.Eye_R) ||
									(part.PartFlag == PortraitConstant.Eye_R && opPart.PartFlag == PortraitConstant.Eye_L) ||
									(part.PartFlag == PortraitConstant.BodyBack && opPart.PartFlag == PortraitConstant.Body) ||
									(part.PartFlag == PortraitConstant.Body && opPart.PartFlag == PortraitConstant.BodyBack) ||
									(part.PartFlag == PortraitConstant.HairBack && opPart.PartFlag == PortraitConstant.Hair) ||
									(part.PartFlag == PortraitConstant.Hair && opPart.PartFlag == PortraitConstant.HairBack);

				if (oppositePart)
				{
					int lastIndex = part.ResourceName.LastIndexOf('_');
					int opLastIndex = opPart.ResourceName.LastIndexOf('_');

					if (lastIndex == opLastIndex && part.ResourceName.Substr(0, lastIndex) == opPart.ResourceName.Substr(0, lastIndex))
					{
						switch (part.PartFlag)
						{
							case (PortraitConstant.Ear_L):
							case (PortraitConstant.Eye_L):
							case (PortraitConstant.Body):
							case (PortraitConstant.Hair):
								{
									part.OppositePart = opPart;
								}
								break;
							case (PortraitConstant.Ear_R):
							case (PortraitConstant.Eye_R):
							case (PortraitConstant.BodyBack):
							case (PortraitConstant.HairBack):
								{
									opPart.OppositePart = part;
									SavePart(opPart, opPart.ResourceName);
								}
								break;
							default:
								Debug.Fail("Attempting to find opposite for unsupported part.");
								break;
						}
					}
				}
			}
		}

		private void SavePart(PortraitResourceWrapper part, string newName)
		{
			part.Save("res://Resources/PortraitParts/" + newName + ".tres");
		}

		public void SaveAll()
		{
			Node vBox = GetNode("HBoxContainer/ScrollContainer/VBoxContainer");
			for(int i = 0; i < vBox.GetChildCount(); ++i)
			{
				Node child = vBox.GetChild(i);
				if(child is PartInspector partInspector)
				{
					partInspector.OnSavePressed();
				}
			}
		}

		public void OnFinalisePressed()
		{
			SaveAll();
			PortraitParts.Save(PortraitParts.ResourcePath);
		}

		public void OnExportPressed()
		{
			FileDialog fileDialog = new FileDialog();
			AddChild(fileDialog);
			fileDialog.Mode = Godot.FileDialog.ModeEnum.SaveFile;
			fileDialog.PopupExclusive = true;
			fileDialog.PopupCentered(new Vector2(480, 320));
			fileDialog.Connect("file_selected", this, nameof(ExportFileToDir));
			fileDialog.CurrentFile = PortraitParts.ResourcePath;
			fileDialog.Filters = new string[]{"*.tres ; Godot resource files"};
		}

		private void ExportFileToDir(string path)
		{
			PortraitParts.Save(path);
		}

		public void OnLoadPressed()
		{
			FileDialog fileDialog = new FileDialog();
			AddChild(fileDialog);
			fileDialog.Mode = Godot.FileDialog.ModeEnum.OpenFile;
			fileDialog.PopupExclusive = true;
			fileDialog.PopupCentered(new Vector2(480, 320));
			fileDialog.Connect("file_selected", this, nameof(LoadSetFromPath));
			fileDialog.CurrentFile = "Resources/PortraitSet.tres";
			fileDialog.Filters = new string[]{"*.tres ; Godot resource files"};
		}

		private void LoadSetFromPath(string path)
		{
			PortraitParts = new PortraitPartSetWrapper(GD.Load<Godot.Object>(path));
			RefreshPartList();
		}
	}
}
