using System;
using System.Diagnostics;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Resources;
using static Godot.ResourceSaver;

namespace ProjectArcadeAssphalt.Addons.Widgets
{
	[Tool]
	public class PartInspector : PanelContainer
	{

		public PortraitConstant[] TypeOrder { get; } = new PortraitConstant[]
															{
																PortraitConstant.Background,
																PortraitConstant.GangIcon,
																PortraitConstant.Head,
																PortraitConstant.Eye_L,
																PortraitConstant.Eye_R,
																PortraitConstant.Ear_L,
																PortraitConstant.Ear_R,
																PortraitConstant.Nose,
																PortraitConstant.Mouth,
																PortraitConstant.Hair,
																PortraitConstant.Body,
																PortraitConstant.BodyBack,
																PortraitConstant.EnforcerBadge,
																PortraitConstant.Accessory,
																PortraitConstant.HairBack,
																PortraitConstant.Hat,
															};

		[Export]
		public NodePath InfoContainerPath { get => InfoContainer.TargetPath; set { InfoContainer.TargetPath = value; } }
		public LookupNode<Container> InfoContainer { get; set; } = new LookupNode<Container>("FullContainer/TopContainer/InfoContainer");

		[Export]
		public NodePath AlertPath { get => Alert.TargetPath; set { Alert.TargetPath = value; } }
		public LookupNode<TextureRect> Alert { get; set; } = new LookupNode<TextureRect>("FullContainer/TopContainer/Image/Alert");
		[Export]
		public NodePath DisabledPath { get => Disabled.TargetPath; set { Disabled.TargetPath = value; } }
		public LookupNode<TextureRect> Disabled { get; set; } = new LookupNode<TextureRect>("FullContainer/TopContainer/Image/Disabled");

		[Export]
		public NodePath ButtonContainerPath { get => ButtonContainer.TargetPath; set { ButtonContainer.TargetPath = value; } }
		public LookupNode<Container> ButtonContainer { get; set; } = new LookupNode<Container>("FullContainer/TopContainer/ButtonContainer");

		[Export]
		public NodePath PartImagePath { get => PartImage.TargetPath; set { PartImage.TargetPath = value; } }
		public LookupNode<TextureRect> PartImage { get; set; } = new LookupNode<TextureRect>("FullContainer/TopContainer/Image");

		[Export]
		public NodePath MandatoryTabPath { get => MandatoryTab.TargetPath; set { MandatoryTab.TargetPath = value; } }
		public LookupNode<Container> MandatoryTab { get; set; } = new LookupNode<Container>("FullContainer/BottomContainer/Tabs/Mandatory");

		[Export]
		public NodePath DisabledTabPath { get => DisabledTab.TargetPath; set { DisabledTab.TargetPath = value; } }
		public LookupNode<Container> DisabledTab { get; set; } = new LookupNode<Container>("FullContainer/BottomContainer/Tabs/Disabled");
		public PortraitResourceWrapper _part = null;
		public PortraitResourceWrapper Part
		{
			get { return _part; }
			set
			{
				_part = value;
				RefreshWidget();
				_part.PartChangedEvent += RefreshWidget;
			}
		}
		public PortraitPartSetWrapper PartSet { get; set; } = null;

		public bool RevertNextFrame { get; set; } = false;

		public override void _EnterTree()
		{
			AddChild(InfoContainer);
			AddChild(Alert);
			AddChild(Disabled);
			AddChild(ButtonContainer);
			AddChild(PartImage);
			AddChild(MandatoryTab);
			AddChild(DisabledTab);
		}

		public override void _Ready()
		{
			ConnectToSignals();
		}

		private void ConnectToSignals()
		{
			Debug.Assert(MandatoryTab.TargetNodeExists);
			Debug.Assert(DisabledTab.TargetNodeExists);
			int index = 0;
			foreach (PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
			{
				if (pFlag != PortraitConstant.Background)
				{
					Godot.Collections.Array binds = new Godot.Collections.Array();
					binds.Add(pFlag);
					MandatoryTab.TargetNode.GetChild<CheckBox>(index).Connect("pressed", this, nameof(OnMandatoryFlagUpdated), binds);
					DisabledTab.TargetNode.GetChild<CheckBox>(index).Connect("pressed", this, nameof(OnDisabledFlagUpdated), binds);
					++index;
				}
			}
		}
		private void OnMandatoryFlagUpdated(PortraitConstant index)
		{
			Part.MandatoryPartsFlags ^= index;
		}

		private void OnDisabledFlagUpdated(PortraitConstant index)
		{
			Part.DisabledPartsFlags ^= index;
		}
		private void OnFormChanged()
		{
			RefreshWidget();
			Alert.TargetNode.Visible = true;
		}

		private void OnTypeChanged(int newSelected)
		{
			Part.PartFlag = TypeOrder[newSelected];
		}

		private void OnZIndexChanged(int newSelected)
		{
			Part.PartZOffset = -2 + newSelected;
		}

		public override void _Process(float delta)
		{
			if (RevertNextFrame)
			{
				RefreshWidget();
				RevertNextFrame = false;
			}
		}

		public void RefreshWidget()
		{
			if (IsInsideTree())
			{
				LoadImage();
				LoadInfo();
				LoadFlags();
				OnEnableStatusUpdated();
			}
			else
			{
				RevertNextFrame = true;
			}
		}

		public void OnDeletePressed()
		{
			bool newVal = !Part.PartEnabled;
			Part.PartEnabled = newVal;
		}

		private void OnEnableStatusUpdated()
		{
			if (Part.PartEnabled)
			{
				ButtonContainer.TargetNode.GetNode<Button>("Delete").Text = "Disable";
				Disabled.TargetNode.Visible = false;
			}
			else
			{
				ButtonContainer.TargetNode.GetNode<Button>("Delete").Text = "Enabled";
				Disabled.TargetNode.Visible = true;
			}
		}

		private void LoadImage()
		{
			Debug.Assert(PartImage.TargetNodeExists);
			if (Part.PartTexture != null)
			{
				PartImage.TargetNode.Texture = Part.PartTexture;
			}
		}

		private void LoadFlags()
		{
			Debug.Assert(MandatoryTab.TargetNodeExists);
			Debug.Assert(DisabledTab.TargetNodeExists);
			// i = 1 to skip background
			int index = 0;
			foreach (PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
			{
				if (pFlag != PortraitConstant.Background)
				{
					if ((Part.MandatoryPartsFlags & pFlag) > 0)
					{
						MandatoryTab.TargetNode.GetChild<CheckBox>(index).Pressed = true;
					}
					else
					{
						MandatoryTab.TargetNode.GetChild<CheckBox>(index).Pressed = false;
					}

					if ((Part.DisabledPartsFlags & pFlag) > 0)
					{
						DisabledTab.TargetNode.GetChild<CheckBox>(index).Pressed = true;
					}
					else
					{
						DisabledTab.TargetNode.GetChild<CheckBox>(index).Pressed = false;
					}
					++index;
				}
			}
		}

		private void LoadInfo()
		{
			Debug.Assert(InfoContainer.TargetNodeExists);
			Debug.Assert(ButtonContainer.TargetNodeExists);

			InfoContainer.TargetNode.GetNode<Label>("Index").Text = "Index: " + Part.PartIndex;
			InfoContainer.TargetNode.GetNode<Label>("Name").Text = "Name: " + Part.ResourceName;
			if (Part.PartTexture != null)
			{
				InfoContainer.TargetNode.GetNode<Button>("Image").Text = "Image: " + System.IO.Path.GetFileName(Part.PartTexture.ResourcePath);
			}
			else
			{
				InfoContainer.TargetNode.GetNode<Button>("Image").Text = "Image: Null";
			}
			if (Part.PartScene != null)
			{
				InfoContainer.TargetNode.GetNode<Button>("Scene").Text = "Scene: " + System.IO.Path.GetFileName(Part.PartScene.ResourcePath);
			}
			else
			{
				InfoContainer.TargetNode.GetNode<Button>("Scene").Text = "Scene: Null";
			}
			if (Part.OppositePart != null)
			{
				InfoContainer.TargetNode.GetNode<Button>("Opposite").Text = "Paired Part: " + System.IO.Path.GetFileName(Part.OppositePart.ResourceName);
			}
			else
			{
				InfoContainer.TargetNode.GetNode<Button>("Opposite").Text = "Paired Part: Null";
			}

			int index = Array.FindIndex(TypeOrder, (PortraitConstant pFlag) => { return pFlag == Part.PartFlag; });

			ButtonContainer.TargetNode.GetNode<OptionButton>("Type").Select(index);
			ButtonContainer.TargetNode.GetNode<OptionButton>("ZIndex").Select(2 + Part.PartZOffset);
		}

		public void OnChangeImagePressed()
		{
			FileDialog fileDialog = new FileDialog();
			AddChild(fileDialog);
			fileDialog.Mode = Godot.FileDialog.ModeEnum.OpenFile;
			fileDialog.PopupExclusive = true;
			fileDialog.PopupCentered(new Vector2(960, 512));
			fileDialog.Filters = new string[] { "*.png ; Image resource" };
			fileDialog.Connect("file_selected", this, nameof(OnChangeImage));
		}

		private void OnChangeImage(string file)
		{
			if (file == null)
			{
				Part.PartScene = null;
			}
			else if (GD.Load(file) is Texture image)
			{

				Part.PartTexture = image;
			}
			else
			{
				Part.PartScene = null;
			}

			PartImage.TargetNode.Texture = Part.PartTexture;
		}

		public void OnChangeScenePressed()
		{
			FileDialog fileDialog = new FileDialog();
			AddChild(fileDialog);
			fileDialog.Mode = Godot.FileDialog.ModeEnum.OpenFile;
			fileDialog.PopupExclusive = true;
			fileDialog.PopupCentered(new Vector2(960, 512));
			fileDialog.Filters = new string[] { "*.scn ; Assphalt Part Scene" };
			fileDialog.Connect("file_selected", this, nameof(OnChangeScene));
		}
		private void OnChangeScene(string file)
		{
			if (file == null)
			{
				Part.PartScene = null;
			}
			else if (GD.Load(file) is PackedScene scene)
			{

				Part.PartScene = scene;
			}
			else
			{
				OnChangeScene(null);
			}
		}
		public void OnChangeOppositePressed()
		{
			PopupMenu menu = InfoContainer.TargetNode.GetNode<MenuButton>("Opposite").GetPopup();
			foreach (Godot.Object unwrapped in PartSet)
			{
				PortraitResourceWrapper part = new PortraitResourceWrapper(unwrapped);
				if (part.PartIndex != Part.PartIndex)
				{
					menu.AddIconItem(part.PartTexture, part.ResourceName, part.PartIndex);
				}
			}

			menu.Connect("id_pressed", this, nameof(OnChangeOpposite), null, (uint)ConnectFlags.Oneshot);
			// FileDialog fileDialog = new FileDialog();
			// AddChild(fileDialog);
			// fileDialog.Mode = Godot.FileDialog.ModeEnum.OpenFile;
			// fileDialog.PopupExclusive = true;
			// fileDialog.PopupCentered(new Vector2(960, 512));
			// fileDialog.Filters = new string[] { "*.tres ; Assphalt Part Resource" };
			// fileDialog.Connect("file_selected", this, nameof(OnChangeOpposite));

		}

		private void OnChangeOpposite(int selectedOpposite)
		{
			if (selectedOpposite == -1)
			{
				Part.OppositePart = null;
			}
			else
			{
				PortraitResourceWrapper toPart = null;
				foreach (Godot.Object unwrapped in PartSet)
				{
					toPart = new PortraitResourceWrapper(unwrapped);
					if (toPart.PartIndex == selectedOpposite)
					{
						break;
					}
				}

				PortraitResourceWrapper fromPart = Part;

				// We always connect left to right or body to back
				switch (fromPart.PartFlag)
				{
					case PortraitConstant.Ear_R:
					case PortraitConstant.Eye_R:
					case PortraitConstant.BodyBack:
					case PortraitConstant.HairBack:
						{
							fromPart = toPart;
							toPart = Part;
						}
						break;
				}

				// Make sure there's no circular dependencies (Only goes 1 deep)
				if (fromPart != null && toPart.OppositePart == fromPart)
				{
					toPart.OppositePart = null;
				}

				// if (file == null)
				// {
				//     InfoContainer.TargetNode.GetNode<Button>("Opposite").Text = "Paired Part: Null";
				//     PendingOppositeFile = null;
				// }
				// else
				// {
				fromPart.OppositePart = toPart;
			}
			// }
		}

		internal void OnSavePressed()
		{
			if (Alert != null && Alert.TargetNodeExists)
			{
				Alert.TargetNode.Visible = false;
			}
		}

		public void OnNullImage()
		{
			OnChangeImage(null);
		}

		public void OnNullScene()
		{
			OnChangeScene(null);
		}

		public void OnNullOpposite()
		{
			OnChangeOpposite(-1);
		}
	}
}
