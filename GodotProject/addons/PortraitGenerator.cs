using Godot;
using System;
namespace ProjectArcadeAssphalt.Addons
{
    [Tool]
    public class PortraitGenerator : EditorPlugin
    {
        public PackedScene MainScene { get; set; } = GD.Load<PackedScene>("res://addons/PortraitGeneratorUI.tscn");
        public Control MainSceneInstance { get; protected set; } = null;
        public override void _Ready()
        {
            MainSceneInstance = MainScene.Instance() as Control;
            GetEditorInterface().GetEditorViewport().AddChild(MainSceneInstance);

            MakeVisible(false);
        }
        public override bool HasMainScreen()
        {
            return true;
        }

        public override void _ExitTree()
        {
            if (MainSceneInstance != null)
            {
                MainSceneInstance.QueueFree();
            }
        }

        public override void MakeVisible(bool visible)
        {
            if (MainSceneInstance != null)
            {
                MainSceneInstance.Visible = visible;
            }
        }

        public override string GetPluginName()
        {
            return "Portrait Part Importer";
        }


        public override Texture GetPluginIcon()
        {
            // Must return some kind of Texture for the icon.
            return GetEditorInterface().GetBaseControl().GetIcon("Node", "EditorIcons");
        }
    }
}