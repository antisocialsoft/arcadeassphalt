using Godot;
using System;
namespace ProjectArcadeAssphalt.Addons
{
    [Tool]
    public class BountyViewer : EditorPlugin
    {
        public PackedScene MainScene { get; set; } = GD.Load<PackedScene>("res://addons/BountyViewer/BountyViewer.tscn");
        public Control MainSceneInstance { get; protected set; } = null;
        public override void _Ready()
        {
            MainSceneInstance = MainScene.Instance() as Control;
            // GetEditorInterface().GetEditorViewport().AddChild(MainSceneInstance);
            AddControlToDock(DockSlot.LeftBr, MainSceneInstance);
            MakeVisible(false);
        }
        // public override bool HasMainScreen()
        // {
        //     return true;
        // }

        public override void _ExitTree()
        {
            if (MainSceneInstance != null)
            {
                RemoveControlFromDocks(MainSceneInstance);
                MainSceneInstance.QueueFree();
                MainSceneInstance = null;
            }
        }

        public override void MakeVisible(bool visible)
        {
            if (MainSceneInstance != null)
            {
                MainSceneInstance.Visible = visible;
            }
        }

        public override string GetPluginName()
        {
            return "Bounty Viewer";
        }


        public override Texture GetPluginIcon()
        {
            // Must return some kind of Texture for the icon.
            return GetEditorInterface().GetBaseControl().GetIcon("Node", "EditorIcons");
        }
    }
}