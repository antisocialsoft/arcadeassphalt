using System;
using System.Collections.Generic;
using System.Diagnostics;
using Godot;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Portraits;
using ProjectArcadeAssphalt.Resources;

namespace ProjectArcadeAssphalt.Addons
{
	[Tool]
	public class BountyLoaderUI : HBoxContainer
	{
		protected Dictionary<PortraitConstant, List<PortraitResourceWrapper>> PortraitLookup { get; } = new Dictionary<PortraitConstant, List<PortraitResourceWrapper>>();

		protected Dictionary<PortraitConstant, int> PartIndices { get; } = new Dictionary<PortraitConstant, int>();

		protected Dictionary<PortraitConstant, bool> NullableParts { get; } = new Dictionary<PortraitConstant, bool>()
		{
			{PortraitConstant.Background, false},
			{PortraitConstant.GangIcon, false},
			{PortraitConstant.Head, false},
			{PortraitConstant.Eye_L, false},
			{PortraitConstant.Eye_R, false},
			{PortraitConstant.Ear_L, false},
			{PortraitConstant.Ear_R, false},
			{PortraitConstant.Nose, false},
			{PortraitConstant.Mouth, false},
			{PortraitConstant.Hair, true},
			{PortraitConstant.Body, true},
			{PortraitConstant.BodyBack, false},
			{PortraitConstant.EnforcerBadge, true},
			{PortraitConstant.Accessory, true},
			{PortraitConstant.HairBack, false},
			{PortraitConstant.Hat, true},
		};


		protected Dictionary<PortraitConstant, Stack<PortraitAttachmentPoint>> AttachmentPoints { get; } = new Dictionary<PortraitConstant, Stack<PortraitAttachmentPoint>>();

		protected PortraitConstant ActiveParts { get; set; } = PortraitConstant.Background;

		protected PortraitConstant DisabledParts { get; set; } = 0;

		protected Control MainScene { get; set; } = null;

		protected PortraitPartSetWrapper PortraitParts { get; set; } = null;

		protected PortraitConstant NotSelectableParts {get;set;} = PortraitConstant.Ear_R | PortraitConstant.Eye_R | PortraitConstant.BodyBack | PortraitConstant.HairBack;

		public override void _EnterTree()
		{
			PortraitParts = new PortraitPartSetWrapper(GD.Load<Godot.Object>("res://Resources/PortraitSet.tres"));

			VBoxContainer leftButtonContainer = GetNode<VBoxContainer>("LeftButtonContainer");
			VBoxContainer rightButtonContainer = GetNode<VBoxContainer>("RightButtonContainer");

			foreach ( PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
			{
                PartIndices[pFlag] = (NullableParts[pFlag])? -1 : 0;
				AttachmentPoints[pFlag] = new Stack<PortraitAttachmentPoint>();
                PortraitLookup[pFlag] = new List<PortraitResourceWrapper>();

                if ((pFlag & NotSelectableParts) > 0)
                {
                    continue;
                }

				Button leftButton = new Button();
				leftButton.Text = "< " + pFlag.ToString();
				leftButtonContainer.AddChild(leftButton);
				Godot.Collections.Array binds = new Godot.Collections.Array();
				binds.Add(pFlag);
				leftButton.Connect("pressed", this, nameof(LowerIndex), binds);

				Button rightButton = new Button();
				rightButton.Text = pFlag.ToString() + " >";
				rightButtonContainer.AddChild(rightButton);
				rightButton.Connect("pressed", this, nameof(RaiseIndex), binds);
			}

			foreach (Godot.Object unwrapped in PortraitParts)
			{
				PortraitResourceWrapper part = new PortraitResourceWrapper(unwrapped);
				if(part.PartEnabled)
				{
					PortraitLookup[part.PartFlag].Add(part);
				}
			}

			RebuildPortrait();
		}

		private void RebuildPortrait()
		{
			if (MainScene != null)
			{
				MainScene.QueueFree();
				MainScene.GetParent().RemoveChild(MainScene);
				MainScene = null;
			}

			foreach ( PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
			{
				AttachmentPoints[pFlag].Clear();
				if(PartIndices[pFlag] == -1)
				{
					ActiveParts &= ~pFlag;
				}
			}
			ActiveParts &= ~NotSelectableParts;

			PortraitConstant newActive = 0;
			PortraitConstant disabledParts = 0;

			foreach ( PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
			{			
				if((ActiveParts & pFlag) > 0)
				{
                    CalculateActiveParts(LookupPart(pFlag), ref newActive, ref disabledParts);
				}
			}

			PortraitConstant toActivate = (newActive | ActiveParts) & ~disabledParts;
			DisabledParts = disabledParts;
			ActiveParts = 0;
			// Apply background to apply everything again
			while (toActivate > 0)
			{
				foreach ( PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
				{
					if ((toActivate & pFlag) > 0)
					{
						if(PartIndices[pFlag] < 0)
						{
							PartIndices[pFlag] = 0;
						}

						if (ApplyPart(LookupPart(pFlag)))
						{
							toActivate &= ~pFlag;
						}
					}
				}
			}


		}
		private void CalculateActiveParts(PortraitResourceWrapper part, ref PortraitConstant currentActive, ref PortraitConstant currentDisabled)
		{
			if(part == null)
			{
				return;
			}
			currentDisabled |= (PortraitConstant)part.DisabledPartsFlags;

			foreach ( PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
			{
				if ((pFlag & currentDisabled) == 0 &&
					(pFlag & currentActive) == 0 &&
					(part.MandatoryPartsFlags & pFlag) > 0)
				{
					// This part is mandatory and not disabled
					currentActive |= pFlag;
					Debug.Assert(PortraitLookup[pFlag].Count > 0);
					PortraitResourceWrapper mandatoryPart = LookupPart(pFlag);
					CalculateActiveParts(mandatoryPart, ref currentActive, ref currentDisabled);
				}
			}

			if (part.OppositePart != null)
			{
				currentActive |= part.OppositePart.PartFlag;
				SetPartIndexTo(part.OppositePart);
				// We also want to enable and disable everything they want to enable
				CalculateActiveParts(part.OppositePart, ref currentActive, ref currentDisabled);
			}
		}

        private void SetPartIndexTo(PortraitResourceWrapper oppositePart)
        {
			IList<PortraitResourceWrapper> partList = PortraitLookup[oppositePart.PartFlag];
            for(int i = 0; i < partList.Count; ++i)
			{
				PortraitResourceWrapper part = partList[i];
				if(part.PartIndex == oppositePart.PartIndex)
				{
					PartIndices[oppositePart.PartFlag] = i;
				}
			}
        }

        private PortraitResourceWrapper LookupPart(PortraitConstant pFlag)
		{
			int index = PartIndices[pFlag];
			if(index < 0)
			{
				return null;
			}
			else
			{
				return PortraitLookup[pFlag][index];
			}
		}

		private bool IsPartActive(PortraitResourceWrapper part)
		{
			return (part.PartFlag & ActiveParts) > 0;
		}

		private bool ApplyPart(PortraitResourceWrapper part)
		{
			if(part == null)
			{
				// No part to activate, so we'll just say it's active
				return true;
			}
			// If there's a part scene, we should add it here
			if (part.PartScene != null)
			{
				PackedScene scene = part.PartScene;
				Control partScene = scene.Instance() as Control;

				if (MainScene == null)
				{
					MainScene = partScene;
					GetNode<Control>("CentralContainer/BountyParent/BountyTag/PortraitAttachment/").AddChild(MainScene);
				}
				else
				{
					PortraitAttachmentPoint sceneAttach = GetAttachmentPoint(part.PartFlag);
					Debug.Assert(sceneAttach != null, "Attachment point does not exist for part type: " + part.PartFlag);
					sceneAttach.AddChild(partScene);
				}

				for (int childIdx = 0; childIdx < partScene.GetChildCount(); ++childIdx)
				{
					PortraitAttachmentPoint child = partScene.GetChild(childIdx) as PortraitAttachmentPoint;
					if (child != null)
					{
						foreach ( PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
						{
							if ((child.AttachmentType & (uint)pFlag) > 0)
							{
								AttachmentPoints[pFlag].Push(child);
							}
						}
					}
				}

				PortraitAttachmentPoint attach = GetAttachmentPoint(part.PartFlag);
				Debug.Assert(attach != null, "Attachment point does not exist in PartScene for part type: " + part.PartFlag);
				TextureRect partSprite = new TextureRect();
				partSprite.Texture = part.PartTexture;
                partSprite.StretchMode = TextureRect.StretchModeEnum.Scale;
				attach.AddChild(partSprite);
				ActiveParts |= part.PartFlag;
			}
			else
			{
				PortraitAttachmentPoint attach = GetAttachmentPoint(part.PartFlag);
				if (attach == null)
				{
					return false;
				}
				TextureRect partSprite = new TextureRect();
				partSprite.Texture = part.PartTexture;
                partSprite.StretchMode = TextureRect.StretchModeEnum.Scale;
				attach.AddChild(partSprite);
				ActiveParts |= part.PartFlag;
			}

			if(part.PartZOffset != 0)
			{
				PortraitAttachmentPoint attach = GetAttachmentPoint(part.PartFlag);
				attach.GetParent().MoveChild(attach, attach.GetIndex()+part.PartZOffset);
			}

            // if (part.OppositePart != null && (part.OppositePart.PartFlag & DisabledParts) == 0)
            // {
            //     bool result = ApplyPart(part.OppositePart);
            //     Debug.Assert(result, "Cannot apply opposite part.");
            // }

			return true;
		}

		private PortraitAttachmentPoint GetAttachmentPoint(PortraitConstant partFlag)
		{
			if (AttachmentPoints[partFlag].Count > 0)
			{
				Debug.Assert(AttachmentPoints[partFlag].Peek().GetChildCount() == 0);
				return AttachmentPoints[partFlag].Peek();
			}
			else
			{
				return null;
			}
		}

		public void LowerIndex(PortraitConstant pFlag)
		{
			DisablePart(pFlag);
			int newIndex = PartIndices[pFlag] - 1;

			if ((newIndex == -1 && !NullableParts[pFlag]) || (newIndex < -1))
			{
				newIndex = PortraitLookup[pFlag].Count - 1;
			}

			PartIndices[pFlag] = newIndex;

			ActiveParts |= pFlag;

			RebuildPortrait();
		}

        public void RaiseIndex(PortraitConstant pFlag)
		{
			DisablePart(pFlag);
			int newIndex = PartIndices[pFlag] + 1;

			if (newIndex == PortraitLookup[pFlag].Count)
			{
				if(!NullableParts[pFlag])
				{
					newIndex = 0;
				}
				else
				{
					newIndex = -1;
				}
			}

			PartIndices[pFlag] = newIndex;

			ActiveParts |= pFlag;

			RebuildPortrait();
		}

		
        private void DisablePart(PortraitConstant pFlag)
        {
			PortraitResourceWrapper part = LookupPart(pFlag);
			if(part != null)
			{
				if(part.OppositePart != null) // Disable the opposite part
				{
					ActiveParts &= ~part.OppositePart.PartFlag;
				}
			}
        }

		public void OnRefreshPressed()
		{
			RebuildPortrait();
		}
	}
}
