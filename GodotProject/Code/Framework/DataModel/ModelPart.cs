﻿using AssGameFramework.Components;
using AssGameFramework.Helpers;
using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.DataModel
{

    /// <summary>
    /// ModelPart is an abstract class. It is meant to define an <see cref="EntityModel"/> with common
    /// functionality grouped into a single object. A ModelPart may contain components or timers (both of
    /// which can be 'Handled' by methods on the ModelPart. They may also take reference to more ModelParts
    /// to extend their own functionality (i.e. A PlayerModelPart may keep a reference to a MovementModelPart
    /// and a HealthPart).
    /// </summary>
    public abstract class ModelPart : Node
    {
        /// <summary>
        /// The <see cref="EntityModel"/> this ModelPart is current attached to.
        /// </summary>
        public Model Model { get; private set; } = null;

        public T As<T>() where T : class
        {
            Debug.Assert(this is T, "Interface required by component not implemented");
            return this as T;
        }

        private ProtectedList<NodeComponent> _components = new ProtectedList<NodeComponent>(new List<NodeComponent>());
       
        /// <summary>
        /// Default constructor
        /// </summary>
        public ModelPart()
        {

        }

        public override void _Ready()
        {
            base._Ready();

            base._Ready();
            for (int i = 0; i < GetChildCount(); ++i)
            {
                Node child = GetChild(i);
                if (child is NodeComponent)
                {
                    NodeComponent nodeComp = child as NodeComponent;
                    AddComponent(nodeComp);
                }
            }
        }

        /// <summary>
        /// Called at the end of _Ready for the model, meaning that all models attached via the editor are
        /// now attached and can process interdependencies.
        /// </summary>
        internal virtual void ModelReady()
        {
            
        }

        internal void AddComponent(NodeComponent newComp)
        {
            if(newComp.ModelPart != null)
            {
                if(newComp.ModelPart == this)
                {
                    return;
                }

                newComp.ModelPart.RemoveComponent(newComp);
            }
            _components.Add(newComp);
            newComp.ModelPart = this;
            if(newComp.GetParent() == null)
            {
                AddChild(newComp);
            }
        }

        internal void RemoveComponent(NodeComponent oldComp)
        {
            _components.Remove(oldComp);
            oldComp.ModelPart = null;
            if(oldComp.GetParent() == this)
            {
                RemoveChild(oldComp);
            }
        }

        internal void AttachedToModel(Model newModel)
        {
            //If there's an existing model, we have to remove ourselves
            if(Model != null)
            {
                foreach(NodeComponent entComp in _components.List)
                {
                    Model.RemoveChild(entComp);
                    entComp.OnRemovedFromModel();
                }

                OnRemovedFromModel();
            }

            //Assign new model
            Model = newModel;
            
            if (Model != null)
            {
                foreach (NodeComponent entComp in _components.List)
                {
                    //Model.AddChild(entComp);
                    entComp.OnAttachedToModel();
                }
                OnAttachedToModel();
            }
        }

        public override string _GetConfigurationWarning()
        {
            string error = base._GetConfigurationWarning();

            if(GetParent() != null && !(GetParent() is Model))
            {
                error += "Parent of a ModelPart should be an Model\n";
            }

            return error;
        }

        /// <summary>
        /// Convenience function for handling logic after being attached to a Model (May not be active)
        /// </summary>
        protected virtual void OnAttachedToModel()
        {

        }

        /// <summary>
        /// Convenience function for handling logic after being removed from a model
        /// </summary>
        protected virtual void OnRemovedFromModel()
        {

        }
    };
}
