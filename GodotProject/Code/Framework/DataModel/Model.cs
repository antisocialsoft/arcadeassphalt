using AssGameFramework.Components;
using AssGameFramework.Helpers;
using AssGameFramework.ProxyNodes;
using AssGameFramework.StateMachines;
using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.DataModel
{
    /// <summary>
    /// Model is the main model attached to an <see cref="Entity"/>
    /// It defines what the Entity is by a series of <see cref="ModelPart"/>s
    /// </summary>
    //[Tool]
    public class Model : Node
    {

        private enum SyncMethod
        {
            NONE,
            REMOTE,
            SYNC,
            TARGET
        }
        private Dictionary<Type, ModelPart> PartLookup { get; set; } = new Dictionary<Type, ModelPart>();

        public bool ModelReady {get; private set;} = false;

        /// <summary>
        /// Constructor that initialises the Model with an owning <see cref="Entity"/>
        /// Internal as the Model should only be constructed by the Entity and from within the framework
        /// </summary>
        /// <param name="owner"></param>
        internal Model()
        {
        }

        public override void _Ready()
        {
            base._Ready();
            for (int i = 0; i < GetChildCount(); ++i)
            {
                GD.Print("Checking child index: " + i);
                Node child = GetChild(i);
                if (child is ModelPart)
                {
                    GD.Print("Adding Model Part");
                    AddPart(child as ModelPart);
                }
            }

            ModelReady = true;
            foreach(ModelPart part in PartLookup.Values)
            {
                part.ModelReady();
            }
        }

        /// <summary>
        /// Adds a <see cref="ModelPart"/> to the model. Only one <see cref="ModelPart"/> type can be attached
        /// at any given time (i.e. you can't have two SpriteModelParts attached to one model).
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="ModelPart"/></typeparam>
        /// <param name="modelPart">The <see cref="ModelPart"/> to be added</param>
        /// <param name="ignoreAttach">Ignore the usual attach function calls, this is useful for adding the same model under multiple types</param>
        /// <returns>True if successful, false if the <see cref="ModelPart"/> could not be added</returns>
        public bool AddPart(ModelPart modelPart, bool ignoreAttach = false)
        {
            Type type = modelPart.GetType();
            GD.Print("adding type " + type.Name);
            if (PartLookup.ContainsKey(type))
            {
                Debug.Assert(false, "Component already added to model.");
                return false;
            }

            PartLookup[type] = modelPart;

            if (modelPart.GetParent() != this)
            {
                AddChild(modelPart);
            }

            if (!ignoreAttach)
            {
                modelPart.AttachedToModel(this);
                if(ModelReady)
                {
                    modelPart.ModelReady();
                }
            }

            return true;
        }

        /// <summary>
        /// Remove a <see cref="ModelPart"/> that's referenced by the given type from the model
        /// </summary>
        /// <typeparam name="T">The <see cref="ModelPart"/> type the part was registered under</typeparam>
        /// <returns>True if successful, false if the part was not found</returns>
        public bool RemovePart<T>() where T : ModelPart
        {
            Type type = typeof(T);
            if (GetModelPart<T>(out T modelPart))
            {
                modelPart.AttachedToModel(null);
                PartLookup.Remove(type);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Remove a specific <see cref="ModelPart"/> from the Model, optionally under the the given type T
        /// </summary>
        /// <typeparam name="T">The type the ModelPart was registered under></typeparam>
        /// <param name="toRemove">The ModelPart to remove</param>
        /// <returns>True if successful, false if the ModelPart was not found</returns>
        public bool RemovePart<T>(T toRemove) where T : ModelPart
        {
            Type type = typeof(T);
            if (GetModelPart<T>(out T modelPart))
            {
                return RemovePart<T>((ModelPart)modelPart);
            }

            return false;
        }

        /// <summary>
        /// Remove the given <see cref="ModelPart"/> registered under the type T
        /// </summary>
        /// <typeparam name="T">The type the ModelPart was registered under</typeparam>
        /// <param name="toRemove">The ModelPart to remove</param>
        /// <returns>True if successful, false if the part was not found</returns>
        public bool RemovePart<T>(ModelPart toRemove) where T : ModelPart
        {
            Type type = typeof(T);
            if (GetModelPart<T>(out T modelPart))
            {
                Debug.Assert(modelPart == toRemove, "Component to remove does not match argument");
                modelPart.AttachedToModel(null);
                PartLookup.Remove(type);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Adds the given <see cref="ModelPart"/> to the Model under the given type T.
        /// </summary>
        /// <typeparam name="T">The type to register the model under</typeparam>
        /// <param name="modelPart">The ModelPart to add</param>
        /// <param name="ignoreAttach">Whether to ignore attach events, useful for adding the model multiple times under different types</param>
        /// <returns>True if the ModelPart was successfully added, false if not</returns>
        public bool AddPart<T>(T modelPart, bool ignoreAttach = false) where T : ModelPart
        {
            Type type = typeof(T);
            GD.Print("adding type " + type.Name);
            if (PartLookup.ContainsKey(type))
            {
                Debug.Assert(false, "Component already added to model.");
                return false;
            }

            PartLookup[type] = modelPart;
            if (!ignoreAttach)
            {
                AddChild(modelPart);
                modelPart.AttachedToModel(this);
                if(ModelReady)
                {
                    modelPart.ModelReady();
                }
            }

            return true;
        }

        /// <summary>
        /// Check to see if the <see cref="EntityModel"/> has a particular <see cref="ModelPart"/>
        /// </summary>
        /// <typeparam name="T">The Type the ModelPart may have been registered under</typeparam>
        /// <returns>True if a ModelPart has been registered under the given type</returns>
        public bool HasModelPart<T>() where T : ModelPart
        {
            return PartLookup.ContainsKey(typeof(T));
        }

        /// <summary>
        /// Convenience function for adding state events into a <see cref="StateMachineModelPart{StateType, EventType}"/>.
        /// Not guaranteed to exist.
        /// </summary>
        /// <typeparam name="StateType">The StateType is required to find the StateMachine to add the event into</typeparam>
        /// <typeparam name="EventType">The EventType is used to handle the event passed in (and find the StateMachine)</typeparam>
        /// <param name="evnt">The event to be added to the state machine.</param>
        /// <param name="eventParams">The parameter lookup for the event</param>
        public virtual void AddStateEvent<StateType, EventType>(EventType evnt)
            where StateType : struct, IConvertible, IFormattable, IComparable
            where EventType : class
        {
            AddStateEvent<StateType, EventType>(evnt, SyncMethod.NONE);
        }

        public virtual void AddSyncedStateEvent<StateType, EventType>(EventType evnt)
            where StateType : struct, IConvertible, IFormattable, IComparable
            where EventType : class
        {
            AddStateEvent<StateType, EventType>(evnt, SyncMethod.SYNC);
        }

        public virtual void AddRemoteStateEvent<StateType, EventType>(EventType evnt)
            where StateType : struct, IConvertible, IFormattable, IComparable
            where EventType : class
        {
            AddStateEvent<StateType, EventType>(evnt, SyncMethod.REMOTE);
        }

        public virtual void AddTargettedStateEvent<StateType, EventType>(EventType evnt, int peerTarget)
            where StateType : struct, IConvertible, IFormattable, IComparable
            where EventType : class
        {
            AddStateEvent<StateType, EventType>(evnt, SyncMethod.TARGET, peerTarget);
        }

        private void AddStateEvent<StateType, EventType>(EventType evnt, SyncMethod method, int target = -1)
                    where StateType : struct, IConvertible, IFormattable, IComparable
            where EventType : class
        {
            GetModelPart(out StateMachineModelPart<StateType, EventType> stateMachine);
            Debug.Assert(stateMachine != null, "State machine cannot be null.");
            switch (method)
            {

                case SyncMethod.REMOTE:
                    stateMachine.AddRemoteStateEvent(evnt);
                    break;
                case SyncMethod.SYNC:
                    stateMachine.AddSyncedStateEvent(evnt);
                    break;
                case SyncMethod.TARGET:
                    stateMachine.AddTargettedStateEvent(target, evnt);
                    break;
                case SyncMethod.NONE:
                default:
                    stateMachine.AddStateEvent(evnt);
                    break;
            }

        }

        /// <summary>
        /// Get the <see cref="ModelPart"/> registered under the given type T
        /// </summary>
        /// <typeparam name="T">The type of the ModelPart</typeparam>
        /// <param name="component">The found component (out param)</param>
        /// <returns>True if the component was found</returns>
        public bool GetModelPart<T>(out T component) where T : ModelPart
        {
            if (PartLookup.TryGetValue(typeof(T), out ModelPart foundComp))
            {
                component = (T)foundComp;
                return true;
            }
            else
            {
                component = null;
                return false;
            }
        }

        /// <summary>
        /// Get the <see cref="ModelPart"/> registered under the given type T
        /// </summary>
        /// <typeparam name="T">The type of the ModelPart</typeparam>
        /// <returns>The ModelPart, or null if it's not been found</returns>
        public T GetModelPart<T>() where T : ModelPart
        {
            GetModelPart<T>(out T comp);
            return comp;
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
        }

        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);
        }
    }
}
