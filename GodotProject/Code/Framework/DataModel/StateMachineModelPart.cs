﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using AssGameFramework.StateMachines;
using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;
using System.Runtime.Serialization;

namespace AssGameFramework.DataModel
{
    /// <summary>
    /// Template <see cref="ModelPart"/> for dealing with <see cref="StateMachineModel.StateMachineInstance"/>.
    /// </summary>
    /// <typeparam name="StateType">IConvertible, IFormattable, IComparable type that defines the states</typeparam>
    /// <typeparam name="EventType">IConvertible, IFormattable, IComparable type that defines the events</typeparam>
    public class StateMachineModelPart<StateType, EventType> : ModelPart
        where StateType : struct, IConvertible, IFormattable, IComparable
        where EventType : class
    {
        [Export]
        public StateType CurrentState { get => StateMachine.CurrentState; set{}}
        
        /// <summary>
        /// Get the <see cref="StateMachineModel.StateMachineInstance"/> held by this model.
        /// </summary>
        public StateMachineModel<StateType, EventType, Model>.Instance StateMachine { get; protected set; } = null;
        
        public StateMachineModelPart() : base()
        {
        }

        /// <summary>
        /// Adds a state event to the <see cref="StateMachineModel.StateMachineInstance"/>
        /// </summary>
        /// <param name="evnt">Event to add</param>
        /// <param name="eventParams">Event parameters</param>
        public void AddStateEvent(EventType evnt)
        {
            Debug.Assert(StateMachine != null, "State machine cannot be null.");
            StateMachine.AddEvent(evnt);
        }

        public void AddSyncedStateEvent(EventType evnt)
        {
            AddRemoteStateEvent(evnt);
            AddStateEvent(evnt);
        }

        public void AddRemoteStateEvent(EventType evnt)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, evnt);

                Rpc("SyncStateEvent", ms.ToArray());
            }
        }

        public void AddTargettedStateEvent(int peerID, EventType evnt)
        {
            if(GetTree().NetworkPeer.GetUniqueId() == peerID)
            {
                // We're trying to send to ourself, so bypass the RPC
                AddStateEvent(evnt);
                return;
            }
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, evnt);

                RpcId(peerID, nameof(SyncStateEvent), ms.ToArray());
            }
        }

        [Remote]
        private void SyncStateEvent(Byte[] byteEvent)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                ms.Write(byteEvent, 0, byteEvent.Length);
                ms.Seek(0, SeekOrigin.Begin);

                AddStateEvent(bf.Deserialize(ms) as EventType);
            }
        }

        /// <summary>
        /// Updates the <see cref="StateMachineModel.StateMachineInstance"/>
        /// </summary>
        /// <param name="fixedStep">Delta time since last update</param>
        public override void _PhysicsProcess(float delta)
        {
            Debug.Assert(StateMachine != null, "State machine cannot be null.");
            StateMachine.UpdateStateMachine();
        }
    }
}
