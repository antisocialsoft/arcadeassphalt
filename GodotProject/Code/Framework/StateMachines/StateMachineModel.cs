﻿using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.StateMachines
{
    /// <summary>
    /// StateMachineModel is a singular model that describes a state machine. It also creates instances that use this model.
    /// </summary>
    /// <typeparam name="StateType">IConvertible, IFormattable, IComparable type used for states</typeparam>
    /// <typeparam name="EventType">IConvertible, IFormattable, IComparable type used for events</typeparam>
    /// <typeparam name="DataModel">The model used processing state actions and guards</typeparam>
    public partial class StateMachineModel<StateType, EventType, DataModel>
        where StateType : struct, IConvertible, IFormattable, IComparable
        where EventType : class
    {
        /// <summary>
        /// The default state each Instance should start in
        /// </summary>
        public StateType EntryState { get; protected set; }

        /// <summary>
        /// The transition events between the StateType and other states, including informations on guards
        /// </summary>
        protected Dictionary<StateType, List<StateTransition>> _TransitionLookup = new Dictionary<StateType, List<StateTransition>>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="entryState">The entry state of the StateMachineModel</param>
        /// <param name="instantEvent">The instant event used by the StateMachineModel</param>
        public StateMachineModel(StateType entryState)
        {
            EntryState = entryState;
        }

        /// <summary>
        /// Add a state transition.
        /// </summary>
        /// <param name="fromState">The originating state</param>
        /// <param name="toState">The destination state</param>
        /// <param name="transitionEvent">The event required</param>
        /// <param name="action">The action performed (Optional)</param>
        /// <param name="guardDelegate">The guard required to transition (Optional)</param>
        public void AddStateTransition<T>(StateType fromState, StateType toState, TransitionAction<T> action = null, GuardDelegate<T> guardDelegate = null) where T : EventType
        {
            TransitionAction<EventType> actionWrapper = null;
            GuardDelegate<EventType> guardWrapper = null;

            if (!typeof(T).Equals(typeof(EventType)))
            {
                if (action != null)
                {
                    actionWrapper = (DataModel model, EventType evnt) =>
                    {
                        return action(model, (T)evnt);
                    };
                }

                if (guardDelegate != null)
                {
                    guardWrapper = (DataModel model, EventType evnt) =>
                    {
                        return guardDelegate(model, (T)evnt);
                    };
                }
            }
            else
            {
                actionWrapper = action as TransitionAction<EventType>;
                guardWrapper = guardDelegate as GuardDelegate<EventType>;
            }

            AddTransition(new StateTransition(fromState, toState, typeof(T), actionWrapper, guardWrapper));
        }

        /// <summary>
        /// Add a state transition.
        /// </summary>
        /// <param name="fromState">The originating state</param>
        /// <param name="toState">The destination state</param>
        /// <param name="action">The optional action to be performed</param>
        /// <param name="guardDelegate">The optional guard required to transition</param>
        public void AddStateTransition(StateType fromState, StateType toState, TransitionAction<EventType> action = null, GuardDelegate<EventType> guardDelegate = null)
        {
            AddTransition(new StateTransition(fromState, toState, action, guardDelegate));
        }

        /// <summary>
        /// Create a new instance of the state machine
        /// </summary>
        /// <param name="model">The data model for the instance</param>
        /// <returns>A new instance</returns>
        public virtual Instance CreateInstance(DataModel model)
        {
            return new Instance(model, this);
        }

        /// <summary>
        /// Cycles through an update based on the arguments presented.
        /// </summary>
        /// <param name="currentState">The state to start the cycle from</param>
        /// <param name="model">The model to use</param>
        /// <param name="eventQueue">The events to process</param>
        /// <returns></returns>
        public StateType Update(StateType currentState, DataModel model, Queue<EventType> eventQueue)
        {
            //Check for deadend state
            if (!_TransitionLookup.ContainsKey(currentState))
            {
                //Deadend state, return the current state
                return currentState;
            }
            //Check instant events first
            EventType currentEvent = null;
            Type eventType = null;

            do
            {
                foreach (StateTransition tran in _TransitionLookup[currentState])
                {
                    if (tran.TransitionEvent == null || tran.TransitionEvent.Equals(eventType))
                    {
                        //We're not waiting for any event or we've got an event
                        //If there's no guard, or the guard is true, we can move on
                        if (tran.Guard == null || tran.Guard(model, currentEvent))
                        {
                            //Play the transition action, if true, consume event (Unless it's the instant event)
                            if (tran.Action == null || tran.Action(model, currentEvent))
                            {
                                if (currentEvent != null)
                                {
                                    eventQueue.Dequeue();
                                }
                            }
                            //Update to next state
                            return Update(tran.ToState, model, eventQueue);
                        }
                    }
                }

                //Ignore if this is the first time and it's the instant event
                if (currentEvent != null)
                {
                    //If we get here, the event isn't relevant, so drop it.
                    eventQueue.Dequeue();
                }

                if (eventQueue.Count > 0)
                {
                    currentEvent = eventQueue.Peek();
                    eventType = currentEvent.GetType();
                }
            } while (eventQueue.Count > 0);

            //We haven't moved to a new state, so just stick here.
            return currentState;
        }

        /// <summary>
        /// Add a new <see cref="StateTransition"/>.
        /// </summary>
        /// <param name="transition">The transition to add</param>
        protected void AddTransition(StateTransition transition)
        {
            if (!_TransitionLookup.ContainsKey(transition.FromState))
            {
                _TransitionLookup[transition.FromState] = new List<StateTransition>();
            }

            _TransitionLookup[transition.FromState].Add(transition);
        }
    }

}
