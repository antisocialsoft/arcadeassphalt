using System;
using System.Collections.Generic;
using System.Diagnostics;
using Godot;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.Singletons;

namespace ProjectArcadeAssphalt.AI
{
    public class AIMap : TileMap
    {
        public int MapXOffset { get; set; } = 0;
        public int MapYOffset { get; set; } = 0;
        
        [Export]
        public float UpdateRate {get;set;} = 1.0f/20.0f;
        protected float _updateCounter = 0.0f;

        public AIMapNode this[int x, int y]
        {
            get 
            {
                Debug.Assert(HasCell(x,y));
                return GetNodeForCell(x,y);
            }
        }

        public override void _Ready()
        {
            base._Ready();
            
            Godot.Collections.Array cellPositionArray = GetUsedCells();
            List<Vector2> cellPositions = new List<Vector2>();

            foreach (Vector2 pos in cellPositionArray)
            {
                cellPositions.Add(pos);
            }

            cellPositions.Sort((Vector2 lhs, Vector2 rhs) =>
            {
                int compare = lhs.x.CompareTo(rhs.x);
                if (compare != 0)
                {
                    return compare;
                }
                else
                {
                    return lhs.y.CompareTo(rhs.y);
                }
            });

            MapXOffset = (int)cellPositions[0].x;
            MapYOffset = (int)(-cellPositions[cellPositions.Count - 1].y);
            Position2D currentParent = null;
            int LastX = (int)-cellPositions[0].x;
            foreach (Vector2 pos in cellPositions)
            {
                int x = (int)pos.x;
                int y = (int)pos.y;

                ConvertToNormalisedCoords(ref x, ref y);

                AIMapNode aiNode = new AIMapNode(this);

                if (x != LastX)
                {
                    currentParent = new Position2D();
                    AddChild(currentParent);
                    currentParent.Position = MapToWorld(pos);
                    LastX = x;
                }

                Vector2 localPos = currentParent.ToLocal(ToGlobal(MapToWorld(pos)));
                aiNode.Position = localPos;

                currentParent.AddChild(aiNode);
                currentParent.MoveChild(aiNode, 0);

                //aiNode._label.Text ="" + x +", " + y;
            }
        }

        internal void GetNormalisedPositionAtGlobalPosition(Vector2 position, out int coordX, out int coordY)
        {
            Vector2 mapPos = WorldToMap(ToLocal(position));
            coordX = (int)mapPos.x;
            coordY = (int)mapPos.y;
            ConvertToNormalisedCoords(ref coordX, ref coordY);
        }

        public new Vector2 MapToWorld(Vector2 mapCoords, bool ignoreHalfOfsets = false)
        {
            mapCoords = base.MapToWorld(mapCoords, ignoreHalfOfsets);
            mapCoords += CellSize * 0.5f;
            return mapCoords;
        }

        internal Vector2 GetGlobalPositionAtNormalisedPosition(int x, int y)
        {
            Debug.Assert(HasCell(x,y));

            return GetNodeForCell(x,y).GlobalPosition;
        }

        private void OnNewMapAffector(MapAffectorModelPart newAffector)
        {
            Rect2 area = newAffector.AreaOfInfluence;

            Vector2 normalisedPos = ToLocal(area.Position) % CellSize;

            if(normalisedPos.x < 0)
            {
                normalisedPos.x += CellSize.x;
            }
            if(normalisedPos.y < 0)
            {
                normalisedPos.y += CellSize.y;
            }

            int lowerRangeX = Mathf.FloorToInt((normalisedPos.x - (area.Size.x * 0.5f)) / CellSize.x);
            int lowerRangeY = Mathf.FloorToInt((normalisedPos.y - (area.Size.y * 0.5f)) / CellSize.y);

            int upperRangeX = Mathf.CeilToInt((normalisedPos.x + (area.Size.x * 0.5f)) / CellSize.x);
            int upperRangeY = Mathf.CeilToInt((normalisedPos.y + (area.Size.y * 0.5f)) / CellSize.y);

            Vector2 mapPos = WorldToMap(ToLocal(area.Position));

            for (int x = lowerRangeX; x < upperRangeX; ++x)
            {
                for (int y = lowerRangeY; y < upperRangeY; ++y)
                {
                    int normalCoordX = x + (int)mapPos.x;
                    int normalCoordY = y + (int)mapPos.y;

                    ConvertToNormalisedCoords(ref normalCoordX, ref normalCoordY);
                    if (HasCell(normalCoordX, normalCoordY))
                    {
                        AIMapNode mapNode = GetNodeForCell(normalCoordX, normalCoordY);

                        mapNode.AddAffector(newAffector);
                    }
                }
            }
        }

        public bool HasCell(int x, int y)
        {
            return x >= 0 && y >= 0 && GetChildCount() > x && GetChild(x).GetChildCount() > y;
        }

        public AIMapNode GetNodeForCell(int x, int y)
        {
            Debug.Assert(GetChildCount() > x);
            Debug.Assert(GetChild(x).GetChildCount() > y);
            AIMapNode aiNode = GetChild(x).GetChild(y) as AIMapNode;
            return aiNode;
        }

        public void ConvertToTileCoords(ref int x, ref int y)
        {
            x += MapXOffset;
            y = -y + MapYOffset;
        }

        public void ConvertToNormalisedCoords(ref int x, ref int y)
        {
            x -= MapXOffset;
            y = -y - MapYOffset;
        }

        public override void _PhysicsProcess(float delta)
        {
            if(GameLogic.Instance.PlayerLookup.TargetNodeExists)
            {
                KinematicBody2D player = GameLogic.Instance.PlayerLookup.TargetNode;
            
                float yPos = player.GlobalPosition.y;
                GlobalPosition = new Vector2(GlobalPosition.x, yPos);
            }
            
            _updateCounter += delta;

            while(_updateCounter >= UpdateRate)
            {
                _updateCounter -= UpdateRate;
                Reset();
                foreach (MapAffectorModelPart affector in MapAffectorModelRegistrar.Instance.MapAffectors)
                {
                    OnNewMapAffector(affector);
                }
            }
            
        }

        private void Reset()
        {
            int x = 0;
            int y = 0;
            while(HasCell(x,y))
            {
                while(HasCell(x,y))
                {
                    AIMapNode node = GetNodeForCell(x,y);
                    node.Reset();
                    ++y;
                }
                y = 0;
                ++x;
            }

        }
    }
}