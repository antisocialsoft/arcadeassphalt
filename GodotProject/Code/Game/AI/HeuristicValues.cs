using System;
using System.Diagnostics;

namespace ProjectArcadeAssphalt.AI
{
            public enum HeatType
        {
            FIRST = 0,
            DANGER = 0,
            OBSTACLE,
            TARGET,
            MAX_VALUES,
    };
    public class HeuristicValues
    {

        protected float[] _values = new float[(int)HeatType.MAX_VALUES];
        protected UInt64[] _collisions = new UInt64[(int)HeatType.MAX_VALUES];

        public static readonly HeuristicValues INF = new HeuristicValues(float.PositiveInfinity, UInt64.MaxValue);
        internal static readonly HeuristicValues ZERO = new HeuristicValues(0, UInt64.MaxValue);

        public HeuristicValues(float defaultScore, UInt64 defaultCollision)
        {
            for(HeatType type = HeatType.FIRST; type < HeatType.MAX_VALUES; ++type)
            {
                _values[(int)type] = defaultScore;
                _collisions[(int)type] = defaultCollision;
            }
        }

        public HeuristicValues()
        {

        }

        public void SetHeuristicValue(HeatType valueType, float newValue)
        {
            Debug.Assert(valueType < HeatType.MAX_VALUES);
            _values[(int)valueType] = newValue;
        }

        public void SetCollision(HeatType valueType, UInt64 newCollision)
        {
            Debug.Assert(valueType < HeatType.MAX_VALUES);
            _collisions[(int)valueType] = newCollision;
        }

        public float GetHeuristicValue(HeatType valueType)
        {
            Debug.Assert(valueType < HeatType.MAX_VALUES);
            return _values[(int)valueType];
        }

        public UInt64 GetCollision(HeatType valueType)
        {
            Debug.Assert(valueType < HeatType.MAX_VALUES);
            return _collisions[(int)valueType];
        }

        public static HeuristicValues operator +(HeuristicValues lhs, HeuristicValues rhs)
        {
            for(HeatType type = HeatType.FIRST; type < HeatType.MAX_VALUES; ++type)
            {
            lhs._values[(int)type] += rhs._values[(int)type];
            }
            return lhs;
        }

        public static HeuristicValues operator -(HeuristicValues lhs, HeuristicValues rhs)
        {
            for(HeatType type = HeatType.FIRST; type < HeatType.MAX_VALUES; ++type)
            {
                lhs._values[(int)type] -= rhs._values[(int)type];
            }

            return lhs;
        }
    }
}