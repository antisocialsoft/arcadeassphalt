using System;
using System.Collections.Generic;
using System.Diagnostics;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.AI
{
    public class AIMapNode : Position2D
    {
        public AIMap MapOwner {get; protected set;} = null;
        public HashSet<MapAffectorModelPart> Affectors {get; protected set;} = new HashSet<MapAffectorModelPart>();

        protected float[] _heatCache = new float[(int)HeatType.MAX_VALUES];
        protected bool[] _cacheValid = new bool[(int)HeatType.MAX_VALUES];

//TODO: Remove eventually --->
        public Label _label = null;
        protected Sprite _sprite = null;
//END OF TODO

        public AIMapNode(AIMap owner)
        {

        }

        public override void _Ready()
        {
            _sprite = new Sprite();
            AddChild(_sprite);

            _label = new Label();
            AddChild(_label);
            _label.Align = Label.AlignEnum.Center;
            _label.Valign = Label.VAlign.Center;
            _label.AnchorBottom = 0.5f;
            _label.AnchorTop = -0.5f;
            _label.AnchorLeft = -0.5f;
            _label.AnchorRight = 0.5f;
            _label.GrowHorizontal = Label.GrowDirection.Both;
            _label.GrowVertical = Label.GrowDirection.Both;
            _label.SelfModulate = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            _label.RectScale = new Vector2(0.8f, 0.8f);

            _sprite.Texture = GD.Load<Texture>("res://Assets/AINodeTexture.png");

        }

        public float timer = 100.0f;
        public override void _PhysicsProcess(float delta)
        {
            timer += delta;
            if (timer > 0.5f || true)
            {
                float obstacleValue = GetHeat(HeatType.OBSTACLE);
                //_sprite.Scale = new Vector2(1,1) + (new Vector2(3, 3) * obstacleValue);

                if (obstacleValue < 0)
                {
                    obstacleValue = -obstacleValue;
                }

                float red = 1.0f * obstacleValue;

                obstacleValue = GetHeat(HeatType.DANGER);
                //_sprite.Scale = new Vector2(1,1) + (new Vector2(3, 3) * obstacleValue);

                if (obstacleValue < 0)
                {
                    obstacleValue = -obstacleValue;
                }

                float blue = 1.0f * obstacleValue;

                _label.Text = obstacleValue.ToString("N2");

                obstacleValue = GetHeat(HeatType.TARGET);
                //_sprite.Scale = new Vector2(1,1) + (new Vector2(3, 3) * obstacleValue);

                if (obstacleValue < 0)
                {
                    obstacleValue = -obstacleValue;
                }

                float green = 1.0f * obstacleValue;

                _sprite.Modulate = new Color(red, green, blue, 1.0f);

                //_label.Text = "";

                for (int i = 0; i < _cacheValid.Length; ++i)
                {
                    _cacheValid[i] = false;
                }
                timer = 0.0f;
            }
        }

        public void Reset()
        {
            Affectors.Clear();

            for(int i = 0; i < _cacheValid.Length; ++i)
            {
                _cacheValid[i] = false;
            }
        }

        public float GetHeat(HeatType heatType, uint collisionMask = uint.MaxValue, MapAffectorModelPart ignoreAffector = null)
        {
            if(_cacheValid[(int)heatType])
            {
                //return _heatCache[(int)heatType];
            }
            float heat = 0;
            foreach(MapAffectorModelPart affector in Affectors)
            {
                if(affector != ignoreAffector && affector.AffectorType == heatType)
                {
                uint collisionLayers = affector.CollisionFlags;
                if((collisionLayers & collisionMask) != 0)
                {
                    heat += affector.CalculateHeatValueForPosition(-ToLocal(affector.AreaOfInfluence.Position));
                }
                }
            }
            _heatCache[(int)heatType] = heat;
            _cacheValid[(int)heatType] = true;
            return heat;
        }

        public void AddAffector(MapAffectorModelPart affector)
        {
            Affectors.Add(affector);
        }

        public void RemoveHeat(MapAffectorModelPart affector)
        {
            Affectors.Remove(affector);
        }

        internal void PopulateListWithAffectors(HeatType heatType, float heatThreshold, uint collisionMask, 
                                                ICollection<MapAffectorModelPart> potentialTargets, 
                                                MapAffectorModelPart ignoreAffector = null)
        {
             foreach(MapAffectorModelPart affector in Affectors)
            {
                if(affector != ignoreAffector && affector.AffectorType == heatType)
                {
                uint collisionLayers = affector.CollisionFlags;
                if((collisionLayers & collisionMask) != 0)
                {
                    float heatVal = affector.CalculateHeatValueForPosition(-ToLocal(affector.AreaOfInfluence.Position));

                    if(heatVal > heatThreshold)
                    {
                        potentialTargets.Add(affector);
                    }
                }
                }
            }
        }
    }
}