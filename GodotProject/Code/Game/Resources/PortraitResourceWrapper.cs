using System;
using System.Diagnostics;
using Godot;
using ProjectArcadeAssphalt.GameConstants;

namespace ProjectArcadeAssphalt.Resources
{
    public class PortraitResourceWrapper
    {
        public delegate void PartChangedDelegate();
        public event PartChangedDelegate PartChangedEvent;

        public Godot.Object PortraitResourceObj { get; } = null;

        public PortraitResourceWrapper(Godot.Object resource)
        {
            this.PortraitResourceObj = resource;
        }

        public PortraitConstant PartFlag
        {
            get { return (PortraitConstant)(/*1 <<*/PortraitResourceObj.Get("PartFlag")); }
            set 
            {
                PortraitResourceObj.Set("PartFlag", (int)value);
                PartChangedEvent.Invoke();
                // int setBit = 0;
                // foreach (PortraitConstant pFlag in Enum.GetValues(typeof(PortraitConstant)))
                // {
                //     if ((pFlag & value) > 0)
                //     {
                //         PortraitResourceObj.Set("PartFlag", setBit);
                //         PartChangedEvent.Invoke();
                //         return;
                //     }
                //     ++setBit;
                // }
                // Debug.Fail("Not a valid type");
            }
        }
        public PortraitConstant MandatoryPartsFlags
        {
            get { return (PortraitConstant)PortraitResourceObj.Get("MandatoryPartsFlags"); }
            set { PortraitResourceObj.Set("MandatoryPartsFlags", value); PartChangedEvent?.Invoke(); }
        }
        public PortraitConstant DisabledPartsFlags
        {
            get { return (PortraitConstant)PortraitResourceObj.Get("DisabledPartsFlags"); }
            set { PortraitResourceObj.Set("DisabledPartsFlags", value); PartChangedEvent?.Invoke(); }
        }
        public Texture PartTexture
        {
            get { return PortraitResourceObj.Get("PartTexture") as Texture; }
            set { PortraitResourceObj.Set("PartTexture", value); PartChangedEvent?.Invoke(); }
        }
        public PackedScene PartScene
        {
            get { return PortraitResourceObj.Get("PartScene") as PackedScene; }
            set { PortraitResourceObj.Set("PartScene", value); PartChangedEvent?.Invoke(); }
        }
        public PortraitResourceWrapper OppositePart
        {
            get 
            { 
                if(PortraitResourceObj.Get("OppositePart") is Godot.Object unwrapped)
                {
                    return new PortraitResourceWrapper(unwrapped);
                } 
                else
                {
                    return null;
                }
            }
            set 
            { 
                if(value != null)
                {
                PortraitResourceObj.Set("OppositePart", value.PortraitResourceObj);
                }
                else
                {
                  PortraitResourceObj.Set("OppositePart", value);  
                }
                PartChangedEvent?.Invoke(); 
            }
        }
        public int PartIndex
        {
            get { return (int)PortraitResourceObj.Get("PartIndex"); }
            set { PortraitResourceObj.Set("PartIndex", value); PartChangedEvent?.Invoke(); }
        }

        public string ResourceName
        {
            get { return PortraitResourceObj.Get("resource_name") as string; }
            set { PortraitResourceObj.Set("resource_name", value); PartChangedEvent?.Invoke(); }
        }

        public string ResourcePath
        {
            get { return PortraitResourceObj.Get("resource_path") as string; }
            set { PortraitResourceObj.Set("resource_path", value); PartChangedEvent?.Invoke(); }
        }

        public bool PartEnabled
        {
            get { return (bool)PortraitResourceObj.Get("PartEnabled"); }
            set { PortraitResourceObj.Set("PartEnabled", value); PartChangedEvent?.Invoke(); }
        }

        public int PartZOffset
        {
            get { return (int)PortraitResourceObj.Get("PartZOffset"); }
            set { PortraitResourceObj.Set("PartZOffset", value); PartChangedEvent?.Invoke(); }
        }

        internal void Save(string resourcePath)
        {
            //ResourceSaver.Save(resourcePath, PortraitResourceObj as Resource);
        }
    }
}