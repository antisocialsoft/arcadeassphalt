using Godot;

namespace ProjectArcadeAssphalt.Resources
{
    public class AIDataResource : Resource
    {
        [Export]
        public int VisibilityX {get; set;} = 3;
        [Export]
        public int VisibilityY {get; set;} = 5;
        [Export]
        public float HeuristicChangeMargin {get;set;} = 0.95f;
        [Export]
        public float NeighbourMultiplier { get; set; } = 0.05f;
        [Export]
        public float ObstaclePriority {get;set;} = 1.0f;
        [Export]
        public float ObstacleDropOff { get; set; } = 0.9f;
        [Export]
        public float DangerPriority {get;set;} = 1.0f;
        [Export]
        public float DangerDropOff {get;set;} = 0.75f;
        [Export]
        public float TargetPriority {get;set;} = 0.7f;
        [Export]
        public float TargetDropOff {get;set;} = 0.55f;
        [Export]
        public float TargetIdleSpeed {get;set;} = 150.0f;
        [Export]
        public float AvoidanceThreshold { get; set; } = 0.8f;
        [Export]
        public float TargetThreshold { get; set; } = 0.75f;
        [Export]
        public float IdleSpeedGain { get; set; } = 1.2f;
        [Export]
        public float AttackInaccuracy { get; set; } = 1.5f;
        [Export]
        public float MaxAttackChance { get;  set; } = 0.8f;
        public float MinViableAttackChance { get; set; } = 0.05f;
    }
}