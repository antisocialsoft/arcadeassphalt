using Godot;

namespace ProjectArcadeAssphalt.Resources
{
    public class BikeMovementDataResource : MovementDataResource
    {
        [Export]
        public float ForwardToTurnAccelerationRatio {get;set;} = 2.0f;

        public override float TurnAcceleration {get;set;} = 0.0f;
        public float CalculateTurnAcceleration(float forwardVelocity)
        {
            return Mathf.Abs(forwardVelocity) * ForwardToTurnAccelerationRatio;
        }
    }
}