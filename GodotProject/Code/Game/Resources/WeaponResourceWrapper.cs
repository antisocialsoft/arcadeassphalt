using Godot;

namespace ProjectArcadeAssphalt.Resources
{
            public enum WeaponType
        {
            NONE,
            SHORT,
            LONG
        }
    public class WeaponResourceWrapper
    {
        public Godot.Resource WeaponResource {get;}
        public Texture WeaponTexture { get { return WeaponResource.Get("WeaponTexture") as Texture; } set { WeaponResource.Set("WeaponTexture", value); } }
        public float BaseDamage { get { return (float)WeaponResource.Get("BaseDamage"); } set { WeaponResource.Set("BaseDamage", value); } }
        public Vector2 DamageAreaOffset { get { return (Vector2)WeaponResource.Get("DamageAreaOffset"); } set { WeaponResource.Set("DamageAreaOffset", value); } }
        public WeaponType WeaponType { get { return (WeaponType)WeaponResource.Get("Type"); } set { WeaponResource.Set("Type", value); } }
        public Shape2D AttackShape { get { return WeaponResource.Get("AttackShape") as Shape2D; } set { WeaponResource.Set("AttackShape", value); } }

        public WeaponResourceWrapper(Godot.Resource weaponResource)
        {
           WeaponResource = weaponResource;
        }
    }
}