using Godot;

namespace ProjectArcadeAssphalt.Resources
{
    public class MovementDataResource : Resource
    {
        [Export]
        public virtual float MinForwardSpeed { get; set; } = 0.0f;
        [Export]
        public virtual float MinTurnSpeed { get; set; } = 0.0f;
        [Export]
        public virtual float MaxForwardSpeed { get; set; } = 10.0f;
        [Export]
        public virtual float MaxTurnSpeed { get; set; } = 1.0f;
        [Export]
        public virtual float ForwardAcceleration { get; set; } = 100.0f;
        [Export]
        public virtual float BackwardsAcceleration {get; set; } = 300.0f;
        [Export]
        public virtual float TurnAcceleration { get; set; } = 100.0f;
        [Export]
        public virtual float ForwardDamping { get; set; } = 2.0f;
        [Export]
        public virtual float TurnDamping { get; set; } = 10.0f;
    }
}