using System;
using System.Collections;
using Godot;

namespace ProjectArcadeAssphalt.Resources
{
    public class PortraitPartSetWrapper
    {
        protected Godot.Object PortraitPartSetObj;

        public PortraitPartSetWrapper(Godot.Object portraitSet)
        {
            this.PortraitPartSetObj = portraitSet;
        }

        public Godot.Collections.Array PartList { get { return PortraitPartSetObj.Get("PartSet") as Godot.Collections.Array;}}
        public int NextID { get { return (int)PortraitPartSetObj.Get("NextID");} set { PortraitPartSetObj.Set("NextID", value); } }
        public string ResourcePath { get { return PortraitPartSetObj.Get("resource_path") as string;} set { PortraitPartSetObj.Set("resource_path", value); } }

        public void AddPart(PortraitResourceWrapper part)
        {
            part.PartIndex = NextID++;
            PartList.Add(part.PortraitResourceObj);
        }

        public IEnumerator GetEnumerator()
        {
            return PartList.GetEnumerator();
        }

        internal bool IsDuplicate(string partName)
        {
            foreach (Godot.Object part in PartList)
            {
                PortraitResourceWrapper wrapper = new PortraitResourceWrapper(part);
                if (wrapper.ResourceName == partName)
                {
                    return true;
                }
            }
            return false;
        }

        internal void Save(string resourcePath)
        {
            ResourceSaver.Save(resourcePath, PortraitPartSetObj as Resource);
        }
    }
}