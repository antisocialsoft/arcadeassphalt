extends Resource

enum WeaponType {
	NONE,
	SHORT,
	LONG
}

export (Texture) var WeaponTexture
export (float) var BaseDamage = 1.0
export (Vector2) var DamageAreaOffset
export (WeaponType) var Type
export (Shape2D) var AttackShape

