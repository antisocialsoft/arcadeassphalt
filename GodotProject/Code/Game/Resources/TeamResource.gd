extends Resource
enum TeamName {
	Civilian = 1 << 0,
	Enforcer = 1 << 1,
	Skinners = 1 << 2,
	Hoops = 1 << 3,
	LastGasps = 1 << 4,
	Ringers = 1 << 5,
	Ragers = 1 << 6,
	NeonPunks = 1 << 7,
	Harbingers = 1 << 8,
	Immortals = 1 << 9
}

enum PortraitSet {
	Civilian = 1 << 0,
	Enforcer = 1 << 1,
	Skinners = 1 << 2,
	Hoops = 1 << 3,
	LastGasps = 1 << 4,
	Ringers = 1 << 5,
	Ragers = 1 << 6,
	NeonPunks = 1 << 7,
	Harbingers = 1 << 8,
	Immortals = 1 << 9
}


export(TeamName) var TeamID
export(TeamName, FLAGS) var Enemies
export(TeamName, FLAGS) var Allies
export(PortraitSet, FLAGS) var ImageSet
# TODO: Add team resources reference
