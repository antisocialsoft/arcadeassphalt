// using Godot;
// using ProjectArcadeAssphalt.GameConstants;

// namespace ProjectArcadeAssphalt.Resources
// {
//     public class PortraitResource : Resource
//     {
//         public PortraitResource()
//         {

//         }
//         public PortraitResource(PortraitResource template)
//         {
//             this.PartFlag = template.PartFlag;
//             this.MandatoryPartsFlags = template.MandatoryPartsFlags;
//             this.DisabledPartsFlags = template.DisabledPartsFlags;
//             this.PartTexture = template.PartTexture;
//             this.PartScene = template.PartScene;
//             this.OppositePart = template.OppositePart;
//             this.PartIndex = template.PartIndex;
//         }

//         [Export]
//         public PortraitConstant PartFlag { get; set; } = PortraitConstant.Background;
//         [PortraitFlagAttribute]
//         public uint MandatoryPartsFlags { get; set; } = 0;
//         [PortraitFlagAttribute]
//         public uint DisabledPartsFlags { get; set; } = 0;
//         [Export]
//         public Texture PartTexture { get; set; } = null;
//         [Export]
//         public PackedScene PartScene { get; set; } = null;
//         [Export]
//         public PortraitResource OppositePart { get; set; } = null;
//         [Export]
//         public uint PartIndex { get; internal set; }
//     }
// }