using ProjectArcadeAssphalt.GameConstants;

namespace ProjectArcadeAssphalt.Resources
{
    public class TeamResourceWrapper
    {
        public Godot.Resource TeamResourceObj { get; } = null;

        public TeamName TeamID
        {
            get => (TeamName)TeamResourceObj.Get("TeamID");
            set { TeamResourceObj.Set("TeamID", value); }
        }
        public PortraitSet ImageSet
        {
            get => (PortraitSet)TeamResourceObj.Get("ImageSet");
            set { TeamResourceObj.Set("ImageSet", value); }
        }

        public TeamName Enemies
        {
            get => (TeamName)TeamResourceObj.Get("Enemies");
            set { TeamResourceObj.Set("Enemies", value); }
        }

        public TeamName Allies
        {
            get => (TeamName)TeamResourceObj.Get("Allies");
            set { TeamResourceObj.Set("Allies", value); }
        }

        public TeamResourceWrapper(Godot.Resource resource)
        {
            this.TeamResourceObj = resource;
        }
    }
}

