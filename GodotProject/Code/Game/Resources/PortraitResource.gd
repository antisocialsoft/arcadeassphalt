extends Resource

enum PortraitConstant {
	Background = 1 << 0,
	GangIcon = 1 << 1,
	Head = 1 << 2,
	Eye_L = 1 << 3,
	Eye_R = 1 << 4,
	Ear_L = 1 << 5,
	Ear_R = 1 << 6,
	Nose = 1 << 7,
	Mouth = 1 << 8,
	Hair = 1 << 9,
	Body = 1 << 10,
	BodyBack = 1 << 11,
	EnforcerBadge = 1 << 12,
	Accessory = 1 << 13,
	HairBack = 1 << 14,
	Hat = 1 << 15
}

export (PortraitConstant) var PartFlag = 1
export (PortraitConstant, FLAGS) var MandatoryPartsFlags
export (PortraitConstant, FLAGS) var DisabledPartsFlags
export (Texture) var PartTexture
export (PackedScene) var PartScene
export (Resource) var OppositePart
export (int) var PartIndex
export (bool) var PartEnabled = true
export (int) var PartZOffset = 0

