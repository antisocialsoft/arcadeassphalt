// using System;
// using System.Collections;
// using System.Collections.Generic;
// using Godot;

// namespace ProjectArcadeAssphalt.Resources
// {
//     public class PortraitPartSet : Resource, IEnumerable
//     {
//         [Export]
//         public Godot.Collections.Array<PortraitResource> PartSet { get; set; } = new Godot.Collections.Array<PortraitResource>();
//         [Export]
//         public uint NextID { get; set; } = 0;
//         public void AddPart(PortraitResource part)
//         {
//             part.PartIndex = NextID++;
//             PartSet.Add(part);
//         }

//         public IEnumerator GetEnumerator()
//         {
//             return PartSet.GetEnumerator();
//         }

//         internal bool IsDuplicate(string partName)
//         {
//             foreach (PortraitResource part in PartSet)
//             {
//                 if (part.ResourceName == partName)
//                 {
//                     return true;
//                 }
//             }
//             return false;
//         }
//     }
// }