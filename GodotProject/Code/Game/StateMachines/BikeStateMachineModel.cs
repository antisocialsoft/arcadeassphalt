﻿using AssGameFramework.DataModel;
using AssGameFramework.StateMachines;
using GodotProject.Code.Game.GameConstants;
using ProjectArcadeAssphalt.Misc;
using ProjectArcadeAssphalt.ModelParts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class BikeStateMachineModel : StateMachineModel<BikeState, StateEvent, Model>
    {
        public BikeStateMachineModel() : base(BikeState.DEAD)
        {
            AddStateTransition(BikeState.DEAD, BikeState.ALIVE, SpawnAction);
            AddStateTransition<BikeEvent.SteerEvent>(BikeState.ALIVE, BikeState.ALIVE, SteerAction);
            AddStateTransition<BikeEvent.ChangeSpeed>(BikeState.ALIVE, BikeState.ALIVE, AccelerateAction);
            AddStateTransition<StateEvent.UpdateVelocity>(BikeState.ALIVE, BikeState.ALIVE, UpdateVelocityAction);

            AddStateTransition<BikeEvent.UnseatRider>(BikeState.ALIVE, BikeState.CRASHING, UnseatAction);
        }

        private bool UnseatAction(Model model, BikeEvent.UnseatRider transitionEvent)
        {
            model.GetModelPart(out BikeModelPart bikePart);

            bikePart.RemoveRiderFromRiderPosition();

            return true;
        }

        private bool SpawnAction(Model model, StateEvent transitionEvent)
        {
            model.GetModelPart(out MovementModelPart movementPart);
            movementPart.MovementEnabled = true;

            model.GetModelPart(out BikeModelPart bikePart);

            BikeViewNode viewNode = bikePart.ViewLookup.TargetNode;

            viewNode.FrontSprite.ZIndex = (int)ZLayer.Bike_Front;
            viewNode.BodySprite.ZIndex = (int)ZLayer.Bike_Body;
            viewNode.FrontWheelSprite.ZIndex = (int)ZLayer.Bike_Wheels;
            viewNode.BackWheelSprite.ZIndex = (int)ZLayer.Bike_Wheels;

            return true;
        }

        private bool UpdateVelocityAction(Model model, StateEvent.UpdateVelocity transitionEvent)
        {
            model.GetModelPart(out BikeModelPart bikePart);

            // Alert the rider that the velocity has changed
            if(bikePart.RiderModel != null)
            {
                bikePart.RiderModel.AddStateEvent<RiderState, StateEvent>(transitionEvent);
            }

            return true;
        }

        private bool AccelerateAction(Model model, BikeEvent.ChangeSpeed transitionEvent)
        {
            model.GetModelPart(out MovementModelPart movementPart);

            movementPart.YDirection += transitionEvent.Acceleration;

            return true;
        }

        private bool SteerAction(Model model, BikeEvent.SteerEvent transitionEvent)
        {
            model.GetModelPart(out MovementModelPart movementPart);

            movementPart.XDirection += transitionEvent.Direction;

            return true;    
        }
    }
}
