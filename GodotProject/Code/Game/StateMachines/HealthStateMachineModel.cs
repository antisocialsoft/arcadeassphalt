using System;
using AssGameFramework.DataModel;
using AssGameFramework.StateMachines;
using Godot;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class HealthStateMachineModel : StateMachineModel<HealthState, StateEvent, Model>
    {

        public HealthStateMachineModel() : base(HealthState.DEAD)
        {
            AddStateTransition<HealthEvent.ResetHealth>(HealthState.DEAD, HealthState.ALIVE, ResetHealthAction, NotResetHealthZeroGuard);
            AddStateTransition<HealthEvent.TakeDamage>(HealthState.ALIVE, HealthState.ALIVE, TakeDamageAction);
            AddStateTransition<HealthEvent.HealthZero>(HealthState.ALIVE, HealthState.DEAD, DieAction);
        }

        #region Guards
        private bool NotResetHealthZeroGuard(Model model, HealthEvent.ResetHealth transitionEvent)
        {
            return transitionEvent.NewHealth != 0;
        }
        #endregion Guards

        #region Actions
        private bool ResetHealthAction(Model model, HealthEvent.ResetHealth transitionEvent)
        {
            model.GetModelPart(out HealthModelPart healthPart);

            if(transitionEvent.NewHealth < 0)
            {
                healthPart.CurrentHealth = transitionEvent.NewHealth;    
            }
            else
            {
                healthPart.CurrentHealth = transitionEvent.NewHealth;
            }

            return true;
        }

        private bool TakeDamageAction(Model model, HealthEvent.TakeDamage transitionEvent)
        {
            model.GetModelPart(out HealthModelPart healthPart);

            healthPart.CurrentHealth -= transitionEvent.Damage;

            if(healthPart.CurrentHealth <= 0)
            {
                model.AddStateEvent<HealthState, StateEvent>(new HealthEvent.HealthZero());
                healthPart.InvokeHealthZeroEvent();
            }
            return true;
        }

        private bool DieAction(Model model, HealthEvent.HealthZero transitionEvent)
        {
            return true;
        }
    }
    #endregion Actions
}