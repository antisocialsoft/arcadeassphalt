using System;
using AssGameFramework.DataModel;
using AssGameFramework.StateMachines;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class AIRiderStateMachineModel : StateMachineModel<AIRiderState, StateEvent, Model>
    {
        public AIRiderStateMachineModel() : base(AIRiderState.INACTIVE)
        {
            AddStateTransition<AIRiderEvent.BeginAIEvent>(AIRiderState.INACTIVE, AIRiderState.IDLE, StartTimersAction);

            AddStateTransition<AIRiderEvent.RecalculateMovementEvent>(AIRiderState.IDLE, AIRiderState.IDLE, RecalculationMovementAction);
            AddStateTransition<AIRiderEvent.RecalculateAttackEvent>(AIRiderState.IDLE, AIRiderState.IDLE, RecalculateAttackAction, CanAttack);
            AddStateTransition<AIRiderEvent.RecalculateTargetEvent>(AIRiderState.IDLE, AIRiderState.IDLE, RecalculateTargetAction, CanAttack);

            AddStateTransition<AIRiderEvent.EndAIEvent>(AIRiderState.IDLE, AIRiderState.INACTIVE, StopTimersAction, CanAttack);
        }

        #region Guards
        private bool CanAttack(Model model, AIRiderEvent transitionEvent)
        {
            return true;
        }
#endregion

#region Actions
        private bool StartTimersAction(Model model, AIRiderEvent.BeginAIEvent transitionEvent)
        {
            model.GetModelPart(out AIStateMachineModelPart aiPart);

            aiPart.MovementTimer.Start();
            aiPart.AttackTimer.Start();
            aiPart.TargetTimer.Start();

            return true;
        }

        private bool StopTimersAction(Model model, AIRiderEvent.EndAIEvent transitionEvent)
        {
            model.GetModelPart(out AIStateMachineModelPart aiPart);

            aiPart.MovementTimer.Stop();
            aiPart.AttackTimer.Stop();
            aiPart.TargetTimer.Stop();

            return true;
        }
        private bool RecalculateTargetAction(Model model, AIRiderEvent.RecalculateTargetEvent transitionEvent)
        {
            model.GetModelPart(out AIStateMachineModelPart aiPart);

            aiPart.CalculateTarget();
            
            return true;
        }

        private bool RecalculateAttackAction(Model model, AIRiderEvent.RecalculateAttackEvent transitionEvent)
        {
            model.GetModelPart(out AIStateMachineModelPart aiPart);

            aiPart.CalculateAttackAction();

            return true;
        }

        private bool RecalculationMovementAction(Model model, AIRiderEvent.RecalculateMovementEvent transitionEvent)
        {
            model.GetModelPart(out AIStateMachineModelPart aiPart);

            aiPart.CalculateMovementTarget();

            return true;
        }
#endregion
    }
}