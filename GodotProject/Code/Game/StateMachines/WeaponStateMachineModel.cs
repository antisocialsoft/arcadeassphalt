using System;
using System.Diagnostics;
using AssGameFramework.DataModel;
using AssGameFramework.StateMachines;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class WeaponStateMachineModel : StateMachineModel<WeaponState, StateEvent, Model>
    {
        public WeaponStateMachineModel() : base(WeaponState.DISABLED)
        {
            AddStateTransition<WeaponEvent.UpdateOwner>(WeaponState.DISABLED, WeaponState.IDLE, SetOwnerAction, OwnerIsNotNull);
            AddStateTransition<WeaponEvent.DealDamage>(WeaponState.IDLE, WeaponState.IDLE, DealDamageAction);
            AddStateTransition<WeaponEvent.UpdateOwner>(WeaponState.IDLE, WeaponState.DISABLED, SetOwnerAction, OwnerIsNull);
        }

    #region Guards
        private bool OwnerIsNotNull(Model model, WeaponEvent.UpdateOwner transitionEvent)
        {
            return !OwnerIsNull(model, transitionEvent);
        }

        private bool OwnerIsNull(Model model, WeaponEvent.UpdateOwner transitionEvent)
        {
            return transitionEvent.WeaponOwner == null;
        }
#endregion
        #region Actions
        private bool SetOwnerAction(Model model, WeaponEvent.UpdateOwner transitionEvent)
        {
            model.GetModelPart(out WeaponModelPart weaponPart);

            weaponPart.WeaponOwner = transitionEvent.WeaponOwner;

            return true;
        }

        private bool DealDamageAction(Model model, WeaponEvent.DealDamage transitionEvent)
        {
            model.GetModelPart(out WeaponModelPart weaponPart);
            
            switch(transitionEvent.AttackDir)
            {
                case AttackDirection.LEFT:
                weaponPart.LeftAttackComponent.DealDamage();
                break;
                case AttackDirection.RIGHT:
                weaponPart.RightAttackComponent.DealDamage();
                break;
                default:
                Debug.Fail("Attack not supported.");
                break;
            }

            return true;
        }
        #endregion Actions
    }
}