using System;
using System.Diagnostics;
using AssGameFramework.Helpers;
using AssGameFramework.DataModel;
using AssGameFramework.ProxyNodes;
using AssGameFramework.StateMachines;
using Godot;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.Misc;
using GodotProject.Code.Game.GameConstants;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class RiderStateMachineModel : StateMachineModel<RiderState, StateEvent, Model>
    {
        public RiderStateMachineModel() : base(RiderState.DEAD)
        {
            AddStateTransition<RiderEvent.SpawnEvent>(RiderState.DEAD, RiderState.RIDING, SpawnRiderOnBikeAction, SpawningOnBikeGuard);
            AddStateTransition<RiderEvent.SpawnEvent>(RiderState.DEAD, RiderState.STARTING_TO_RIDE, SpawnRiderNextToBikeAction, SpawningNextToBikeGuard);
            AddStateTransition<RiderEvent.SpawnEvent>(RiderState.DEAD, RiderState.IDLE, SpawnRiderAction, SpawningOffBikeGuard);

            AddStateTransition<RiderEvent.TurnEvent>(RiderState.RIDING, RiderState.RIDING, SteerBikeAction);
            AddStateTransition<RiderEvent.WalkEvent>(RiderState.RIDING, RiderState.RIDING, AccelerateBikeAction);
            AddStateTransition<RiderEvent.UpdateVelocity>(RiderState.RIDING, RiderState.RIDING, UpdateRiderVelocity);

            AddStateTransition<RiderEvent.HoldAttack>(RiderState.RIDING, RiderState.HOLDING_ATTACK, StartAttackAction);

            AddStateTransition<RiderEvent.TurnEvent>(RiderState.HOLDING_ATTACK, RiderState.HOLDING_ATTACK, SteerWithAttackAction);
            AddStateTransition<RiderEvent.WalkEvent>(RiderState.HOLDING_ATTACK, RiderState.HOLDING_ATTACK, AccelerateBikeAction);
            AddStateTransition<RiderEvent.ReleaseAttack>(RiderState.HOLDING_ATTACK, RiderState.ATTACKING, ReleaseAttackAction);

            AddStateTransition<RiderEvent.TurnEvent>(RiderState.ATTACKING, RiderState.ATTACKING, SteerWhileAttackingAction);
            AddStateTransition<RiderEvent.WalkEvent>(RiderState.ATTACKING, RiderState.ATTACKING, AccelerateBikeAction);
            AddStateTransition<RiderEvent.AttackFinished>(RiderState.ATTACKING, RiderState.RIDING, AttackFinishedAction);

            AddStateTransition<HealthEvent.HealthZero>(RiderState.ATTACKING, RiderState.DEAD, DeadAction);
            AddStateTransition<HealthEvent.HealthZero>(RiderState.HOLDING_ATTACK, RiderState.DEAD, DeadAction);
            AddStateTransition<HealthEvent.HealthZero>(RiderState.RIDING, RiderState.DEAD, DeadAction);

            AddStateTransition<RiderEvent.FallOff>(RiderState.DEAD, RiderState.DEAD, FallAction);


        }

        private bool FallAction(Model model, RiderEvent.FallOff transitionEvent)
        {
            model.GetModelPart(out RiderModelPart riderPart);
            riderPart.BikeBody.Model.AddStateEvent<BikeState, StateEvent>(new BikeEvent.UnseatRider());
                        
            model.GetModelPart(out MovementModelPart movePart);
            movePart.MovementEnabled = true;

            RiderViewNode viewNode = riderPart.ViewLookup.TargetNode;

            viewNode.BodySprite.ZIndex = (int)ZLayer.Ground;
            viewNode.HeadSprite.ZIndex = (int)ZLayer.Ground;
            viewNode.LegsSprite.ZIndex = (int)ZLayer.Ground;
            viewNode.WeaponSprite.ZIndex = (int)ZLayer.Ground;
            viewNode.OverlaySprite.ZIndex = (int)ZLayer.Ground;

            return true;
        }

        private bool DeadAction(Model model, HealthEvent.HealthZero transitionEvent)
        {
            model.GetModelPart(out RiderModelPart riderPart);

            string animName = "Fall";
            riderPart.ViewLookup.TargetNode.AnimationPlayer.Play(animName);
            riderPart.AttackHeld = AttackDirection.NONE;

            model.GetModelPart(out MovementModelPart movePart);

            movePart.InputEnabled = false;

            return true;
        }

        #region Actions
        private bool UpdateRiderVelocity(Model model, RiderEvent.UpdateVelocity transitionEvent)
        {
            model.GetModelPart(out MovementModelPart movementPart);

            movementPart.Velocity = transitionEvent.Velocity;

            return true;
        }
        private bool ReleaseAttackAction(Model model, RiderEvent.ReleaseAttack transitionEvent)
        {
            model.GetModelPart(out RiderModelPart riderPart);

            Debug.Assert(riderPart.ViewLookup.TargetNodeExists);

            string animName = (riderPart.AttackHeld == AttackDirection.LEFT) ? "AttackLeft" : "AttackRight";
            riderPart.ViewLookup.TargetNode.AnimationPlayer.Play(animName);
            riderPart.AttackHeld = AttackDirection.NONE;

            return true;
        }

        private bool StartAttackAction(Model model, RiderEvent.HoldAttack transitionEvent)
        {
            BeginAttack(model, transitionEvent.Direction);

            return true;
        }

        private bool SteerWithAttackAction(Model model, RiderEvent.TurnEvent transitionEvent)
        {
            SteerBikeAction(model, transitionEvent);

            if (transitionEvent.StartTurn)
            {
                AttackDirection newAttackDir = (transitionEvent.Direction < 0) ? AttackDirection.LEFT : AttackDirection.RIGHT;
                BeginAttack(model, newAttackDir);
            }

            return true;
        }

        private bool SteerWhileAttackingAction(Model model, RiderEvent.TurnEvent transitionEvent)
        {
            SteerBikeAction(model, transitionEvent);

            return true;
        }
        private bool SteerBikeAction(Model model, RiderEvent.TurnEvent transitionEvent)
        {
            model.GetModelPart(out RiderModelPart riderPart);
            riderPart.BikeBody.Model.AddStateEvent<BikeState, StateEvent>(new BikeEvent.SteerEvent(transitionEvent.Direction));
            return true;
        }

        private bool AccelerateBikeAction(Model model, RiderEvent.WalkEvent transitionEvent)
        {
            model.GetModelPart(out RiderModelPart riderPart);
            riderPart.BikeBody.Model.AddStateEvent<BikeState, StateEvent>(new BikeEvent.ChangeSpeed(transitionEvent.Direction));
            riderPart.CombatLookup.TargetNode.Model.AddStateEvent<WeaponState, StateEvent>(new WeaponEvent.UpdateOwner(riderPart.Model.Owner as IModelProxy));
            model.AddStateEvent<HealthState, StateEvent>(new HealthEvent.ResetHealth());
            return true;
        }
        private bool SpawnRiderAction(Model model, RiderEvent.SpawnEvent transitionEvent)
        {
            throw new NotImplementedException();
        }

        private bool SpawnRiderNextToBikeAction(Model model, RiderEvent.SpawnEvent transitionEvent)
        {
            throw new NotImplementedException();
        }

        private bool SpawnRiderOnBikeAction(Model model, RiderEvent.SpawnEvent transitionEvent)
        {
            SpawnPlayerAction(model);

            Debug.Assert(transitionEvent.Bike is IModelProxy);
            Model bikeModel = transitionEvent.Bike.Model;
            Debug.Assert(bikeModel != null);

            bikeModel.GetModelPart(out BikeModelPart bikePart);

            Node parent = model.Owner.GetParentOrNull<Node>();
            if (parent != null)
            {
                parent.RemoveChild(model.Owner);
            }

            KinematicBody2DProxy rider = model.Owner as KinematicBody2DProxy;
            bikePart.AddRiderToRiderPosition(rider);

            rider.Position = transitionEvent.Position;

            model.GetModelPart(out RiderModelPart riderPart);

            riderPart.BikeBody = transitionEvent.Bike;
            // Disable rider movement, as we wont be moving while riding
            model.GetModelPart(out MovementModelPart movementPart);

            movementPart.MovementEnabled = false;

            RiderViewNode viewNode = riderPart.ViewLookup.TargetNode;

            viewNode.BodySprite.ZIndex = (int)ZLayer.ROB_Body;
            viewNode.HeadSprite.ZIndex = (int)ZLayer.ROB_Head;
            viewNode.LegsSprite.ZIndex = (int)ZLayer.ROB_Legs;
            viewNode.WeaponSprite.ZIndex = (int)ZLayer.ROB_Weapon;
            viewNode.OverlaySprite.ZIndex = (int)ZLayer.ROB_Overlay;

            return true;
        }
        private bool AttackFinishedAction(Model model, RiderEvent.AttackFinished transitionEvent)
        {
            model.GetModelPart(out RiderModelPart riderPart);

            Debug.Assert(riderPart.ViewLookup.TargetNodeExists);

            string animName = "Ride";//(transitionEvent.Direction == AttackDirection.LEFT) ? "RideLeft" : "RideRight";
            riderPart.ViewLookup.TargetNode.AnimationPlayer.Play(animName);

            return true;
        }
        #endregion Action
        #region Guards
        private bool SpawningOffBikeGuard(Model model, RiderEvent.SpawnEvent transitionEvent)
        {
            return transitionEvent.Bike == null;
        }

        private bool SpawningNextToBikeGuard(Model model, RiderEvent.SpawnEvent transitionEvent)
        {
            return (transitionEvent.Bike != null) && (transitionEvent.Riding == false);
        }

        private bool SpawningOnBikeGuard(Model model, RiderEvent.SpawnEvent transitionEvent)
        {
            return (transitionEvent.Bike != null) && (transitionEvent.Riding == true);
        }
        #endregion Guards

        #region CommonActions
        private void SpawnPlayerAction(Model model)
        {
            (model.Owner as Node2D).Visible = true;
        }

        private void BeginAttack(Model model, AttackDirection direction)
        {
            model.GetModelPart(out RiderModelPart riderPart);

            Debug.Assert(riderPart.ViewLookup.TargetNodeExists);
            GD.Print("Attacking: " + direction);
            string animName = (direction == AttackDirection.LEFT) ? "HoldLeft" : "HoldRight";
            riderPart.ViewLookup.TargetNode.AnimationPlayer.Play(animName);
            riderPart.AttackHeld = direction;
        }
        #endregion CommonActions
    }
}