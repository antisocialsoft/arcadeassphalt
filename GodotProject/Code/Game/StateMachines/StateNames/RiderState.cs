namespace ProjectArcadeAssphalt.StateMachines
{
    public enum RiderState
    {
        DEAD,
        IDLE,
        STARTING_TO_RIDE,
        RIDING,
        HOLDING_ATTACK,
        ATTACKING
    }
}