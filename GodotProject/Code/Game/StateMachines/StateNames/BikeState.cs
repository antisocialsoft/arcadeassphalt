﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectArcadeAssphalt.StateMachines
{
    public enum BikeState
    {
        DEAD,
        SPAWNING,
        ALIVE,
        CRASHING,
        CRASHING_SIDE
    }
}
