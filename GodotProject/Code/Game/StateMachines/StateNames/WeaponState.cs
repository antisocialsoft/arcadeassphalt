namespace ProjectArcadeAssphalt.StateMachines
{
    public enum WeaponState
    {
        DISABLED,
        IDLE,
        ATTACKING
    }
}