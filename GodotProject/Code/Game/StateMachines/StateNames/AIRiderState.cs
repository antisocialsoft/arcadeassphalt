namespace ProjectArcadeAssphalt.StateMachines
{
    public enum AIRiderState
    {
        INACTIVE,
        IDLE,
        AGGRESSIVE,
        DEFENSIVE,
        FLEEING
    }
}