using System;
using AssGameFramework.DataModel;
using AssGameFramework.StateMachines;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class TeamStateMachineModel : StateMachineModel<TeamState, StateEvent, Model>
    {
        public TeamStateMachineModel() : base(TeamState.UNASSIGNED)
        {
            AddStateTransition<TeamEvent.AssignNewTeam>(TeamState.UNASSIGNED, TeamState.ASSIGNED, AssignToTeamAction);
            AddStateTransition<TeamEvent.AssignNewTeam>(TeamState.ASSIGNED, TeamState.ASSIGNED, AssignToTeamAction);
        }

        private bool AssignToTeamAction(Model model, TeamEvent.AssignNewTeam transitionEvent)
        {
            model.GetModelPart(out TeamModelPart teamPart);

            teamPart.TeamData = transitionEvent.NewTeamData; 

            return true;
        }
    }
}