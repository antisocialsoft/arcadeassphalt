using System;
using AssGameFramework.DataModel;
using AssGameFramework.StateMachines;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class RiderControlStateMachineModel : StateMachineModel<RiderControlState, StateEvent, Model>
    {
        public RiderControlStateMachineModel() : base(RiderControlState.UNASSIGNED)
        {
            AddStateTransition<RiderControlEvent.SwitchControlType>(RiderControlState.UNASSIGNED, RiderControlState.ASSIGNED, SwitchToAIAction, IsAIControlGuard);
            AddStateTransition<RiderControlEvent.SwitchControlType>(RiderControlState.UNASSIGNED, RiderControlState.ASSIGNED, SwitchToKeyboardAction, IsKeyboardGuard);

            AddStateTransition<RiderControlEvent.SwitchControlType>(RiderControlState.ASSIGNED, RiderControlState.ASSIGNED, SwitchToKeyboardAction, IsKeyboardGuard);
            AddStateTransition<RiderControlEvent.SwitchControlType>(RiderControlState.ASSIGNED, RiderControlState.ASSIGNED, SwitchToAIAction, IsAIControlGuard);
        }

        #region Guards
        private bool IsKeyboardGuard(Model model, RiderControlEvent.SwitchControlType transitionEvent)
        {
            return transitionEvent.PlayerType == PlayerType.KEYBOARD;
        }

        private bool IsAIControlGuard(Model model, RiderControlEvent.SwitchControlType transitionEvent)
        {
            return transitionEvent.PlayerType == PlayerType.AI;
        }

        #endregion Guards

        #region Actions
        private bool SwitchToAIAction(Model model, RiderControlEvent.SwitchControlType transitionEvent)
        {
            model.GetModelPart(out RiderControlStateMachineModelPart controlPart);

            controlPart.ControlComponent = new AIInputComponent();


            model.AddStateEvent<AIRiderState, StateEvent>(new AIRiderEvent.BeginAIEvent());

            return true;
        }

        private bool SwitchToKeyboardAction(Model model, RiderControlEvent.SwitchControlType transitionEvent)
        {
            model.GetModelPart(out RiderControlStateMachineModelPart controlPart);

            controlPart.ControlComponent = new KeyboardInputComponent();

            model.AddStateEvent<AIRiderState, StateEvent>(new AIRiderEvent.EndAIEvent());

            return true;
        }

        #endregion Actions
    }
}