﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;

namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class StateEvent 
    {
        internal class UpdateVelocity : RiderEvent
        {
            public Vector2 Velocity {get;set;} = Vector2.Zero;

            public UpdateVelocity(Vector2 newVelocity)
            {
                Velocity = newVelocity;
            }
        }
    }
}
