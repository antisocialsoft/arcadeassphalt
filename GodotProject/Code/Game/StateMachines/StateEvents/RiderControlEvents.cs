using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class RiderControlEvent : StateEvent
    {
        public class SwitchControlType : RiderControlEvent
        {
            public PlayerType PlayerType {get;set;} = PlayerType.INVALID;
            public SwitchControlType(PlayerType playerType)
            {
                PlayerType = playerType;
            }
        }
    }
}