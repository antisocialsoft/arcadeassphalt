﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class BikeEvent : StateEvent
    {
        public class SteerEvent : BikeEvent
        {
            public float Direction { get; set; } = 0.0f;

            public SteerEvent(float direction) { Direction = direction; }
        }

        public class ChangeSpeed : BikeEvent
        {
            public float Acceleration { get; set; } = 0.0f;

            public ChangeSpeed(float acceleration) { Acceleration = acceleration; }
        }

        public class UnseatRider : BikeEvent
        {
            public UnseatRider()
            {
            }
        }
    }
}
