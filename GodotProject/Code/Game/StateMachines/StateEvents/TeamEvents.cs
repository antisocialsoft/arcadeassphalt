using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Resources;

namespace ProjectArcadeAssphalt.StateMachines
{
    public class TeamEvent : StateEvent
    {
        public class AssignNewTeam : TeamEvent
        {
            public TeamResourceWrapper NewTeamData {get;set;}

            public AssignNewTeam(TeamResourceWrapper newTeamData)
            {
                NewTeamData = newTeamData;
            }
        }
    }
}