namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class AIRiderEvent : StateEvent
    {
        public class BeginAIEvent : AIRiderEvent
        {
            
        }

        public class EndAIEvent : AIRiderEvent
        {
            
        }
        public class RecalculateMovementEvent : AIRiderEvent
        {

        }

        public class RecalculateTargetEvent : AIRiderEvent
        {

        }

        public class RecalculateAttackEvent : AIRiderEvent
        {

        }
    }
}