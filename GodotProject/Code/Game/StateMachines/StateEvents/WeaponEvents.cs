using AssGameFramework.ProxyNodes;

namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class WeaponEvent : StateEvent
    {
        public class DealDamage : WeaponEvent 
        {
            public AttackDirection AttackDir {get;set;} = AttackDirection.NONE;

            public DealDamage(AttackDirection attackDir)
            {
                AttackDir = attackDir;
            }
         }

        internal class UpdateOwner : WeaponEvent
        {
            public IModelProxy WeaponOwner {get;set;} = null;

            public UpdateOwner(IModelProxy newOwner)
            {
                WeaponOwner = newOwner;
            }
        }
    }
}