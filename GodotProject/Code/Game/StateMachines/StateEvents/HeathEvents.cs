namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class HealthEvent : StateEvent
    {
        public class ResetHealth : HealthEvent
        {
            public float NewHealth {get;set;} = -1;

            public ResetHealth(float newHealth) { NewHealth = newHealth; }
            public ResetHealth() {}
        }

        public class TakeDamage : HealthEvent
        {
            public float Damage {get;set;} = 0;

            public TakeDamage(float newDamage)
            {
                Damage = newDamage;
            }
        }

        public class HealthZero : HealthEvent
        {
            public HealthZero() { }
        }
    }
}