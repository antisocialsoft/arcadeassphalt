using AssGameFramework.ProxyNodes;
using Godot;

namespace ProjectArcadeAssphalt.StateMachines
{
    public abstract class RiderEvent : StateEvent
    {
        public class SpawnEvent : RiderEvent
        {
            public Vector2 Position { get; set; } = new Vector2(0, 0);
            public KinematicBody2DProxy Bike { get; set; } = null;
            public bool Riding { get; set; } = false;

            public SpawnEvent(Vector2 position)
            {
                Position = position;
            }

            public SpawnEvent(KinematicBody2DProxy bike, bool riding)
            {
                Bike = bike;
                Riding = riding;
            }
        }

        public class WalkEvent : RiderEvent
        {
            public float Direction { get; set; } = 0.0f;

            public WalkEvent(float dir)
            {
                Direction = dir;
            }
        }

        public class TurnEvent : RiderEvent
        {
            public float Direction { get; set; } = 0.0f;
            public bool StartTurn {get;set;} = true;

            public TurnEvent(float dir, bool startTurn)
            {
                Direction = dir;
                StartTurn = startTurn;
            }
        }

        internal class HoldAttack : RiderEvent
        {
            public AttackDirection Direction {get;set;} = AttackDirection.LEFT;

            public HoldAttack(AttackDirection dir)
            {
                Direction = dir;
            }
        }

        internal class ReleaseAttack : RiderEvent
        {
        }

        internal class AttackFinished : RiderEvent
        {
            public AttackDirection Direction {get;set;} = AttackDirection.LEFT;

            public AttackFinished(AttackDirection dir)
            {
                this.Direction = dir;
            }
        }

        public class FallOff : RiderEvent
        {
            public FallOff() {}
        }
    }
}