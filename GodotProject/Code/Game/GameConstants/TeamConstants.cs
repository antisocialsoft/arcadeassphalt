using System;

namespace ProjectArcadeAssphalt.GameConstants
{
    [Flags]
    public enum TeamName
    {
        Civilian = 1 << 0,
        Enforcer = 1 << 1,
        Skinners = 1 << 2,
        Hoops = 1 << 3,
        LastGasps = 1 << 4,
        Ringers = 1 << 5,
        Ragers = 1 << 6,
        NeonPunks = 1 << 7,
        Harbingers = 1 << 8,
        Immortals = 1 << 9,
    }

    [Flags]
    public enum PortraitSet
    {
        Civilian = 1 << 0,
        Enforcer = 1 << 1,
        Skinners = 1 << 2,
        Hoops = 1 << 3,
        LastGasps = 1 << 4,
        Ringers = 1 << 5,
        Ragers = 1 << 6,
        NeonPunks = 1 << 7,
        Harbingers = 1 << 8,
        Immortals = 1 << 9,
    }
}