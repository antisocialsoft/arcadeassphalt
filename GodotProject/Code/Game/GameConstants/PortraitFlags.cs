using System;
using Godot;

namespace ProjectArcadeAssphalt.GameConstants
{
    // Must match the list order below
    [Flags]
    public enum PortraitConstant
    {
        Background = 1,
        GangIcon = 1 << 1,
        Head  = 1 << 2,
        Eye_L = 1 << 3,
        Eye_R = 1 << 4,
        Ear_L = 1 << 5,
        Ear_R = 1 << 6,
        Nose = 1 << 7,
        Mouth = 1 << 8,
        Hair = 1 << 9,
        Body = 1 << 10,
        BodyBack = 1 << 11,
        EnforcerBadge = 1 << 12,
        Accessory = 1 << 13,
        HairBack = 1 << 14,
        Hat = 1 << 15,
    }
    public class PortraitFlagAttribute : ExportAttribute
    {
        public PortraitFlagAttribute()
        // Flag list must match enum above
            : base(PropertyHint.Flags, "Background,GangIcon,Head,Eye_L,Eye_R,Ear_L,Ear_R,Nose,Mouth,Hair,Body,BodyBack,EnforcerBadge,Accessory,HairBack,Hat")
        { }
    }
}