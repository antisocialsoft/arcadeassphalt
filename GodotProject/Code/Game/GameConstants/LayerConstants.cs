namespace GodotProject.Code.Game.GameConstants
{
    public enum ZLayer
    {
        Ground = 0,
        Bike_Wheels = 1,
        Rider_Legs = 2,
        Bike_Body = 3,
        ROB_Legs = 4,
        Rider_Body = 5,
        ROB_Body = 6,
        Bike_Front = 7,
        Rider_Head = 8,
        ROB_Head = 9,
        Rider_Weapon = 10,
        Rider_Overlay = 11,
        ROB_Weapon = 12,
        ROB_Overlay = 13,
    }
}