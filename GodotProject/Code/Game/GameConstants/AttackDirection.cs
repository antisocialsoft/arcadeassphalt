namespace ProjectArcadeAssphalt
{
    public enum AttackDirection
    {
        NONE,
        LEFT,
        RIGHT,
        NUM_ATTACK_DIRECTIONS
    }
}