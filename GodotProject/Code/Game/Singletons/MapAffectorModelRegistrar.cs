using Godot;
using ProjectArcadeAssphalt.ModelParts;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProjectArcadeAssphalt.Singletons
{
    public class MapAffectorModelRegistrar : Node
    {
        protected HashSet<MapAffectorModelPart> Affectors {get; set;} = new HashSet<MapAffectorModelPart>();
        public IReadOnlyCollection<MapAffectorModelPart> MapAffectors { get { return Affectors; } }

        public static MapAffectorModelRegistrar Instance {get; private set;}

        public delegate void NewMapAffector(MapAffectorModelPart newAffector);
        public event NewMapAffector NewMapAffectorEvent;

        public MapAffectorModelRegistrar()
        {
            Debug.Assert(Instance == null);
            Instance = this;
        }

        ~MapAffectorModelRegistrar()
        {
            Debug.Assert(Instance != null);
            Instance = null;
        }
        public void RegisterMapAffector(MapAffectorModelPart newAffector)
        {
            Affectors.Add(newAffector);
            NewMapAffectorEvent?.Invoke(newAffector);
        }
        public void UnregisterMapAffector(MapAffectorModelPart oldAffector)
        {
            Affectors.Remove(oldAffector);
        }
    }
}