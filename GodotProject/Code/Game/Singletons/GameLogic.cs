using System.Diagnostics;
using AssGameFramework.Helpers;
using ProjectArcadeAssphalt.AI;
using Godot;
using AssGameFramework.ProxyNodes;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.StateMachines;
using ProjectArcadeAssphalt.Resources;

namespace ProjectArcadeAssphalt.Singletons
{
    public class GameLogic : Node
    {
        [Export]
        public NodePath PlayerPath { get => PlayerLookup.TargetPath; set {PlayerLookup.TargetPath = value;}}
        public LookupNode<KinematicBody2DProxy> PlayerLookup {get;set;} = new LookupNode<KinematicBody2DProxy>("Bike/RiderPos/Rider");
        [Export]
        public NodePath AIMapPath { get => AIMapLookup.TargetPath; set {AIMapLookup.TargetPath = value;}}
        public LookupNode<AIMap> AIMapLookup {get;set;} = new LookupNode<AIMap>("Map");
                public static GameLogic Instance {get; private set;}

                public GameLogic()
                {
                    Debug.Assert(Instance == null);
                    Instance = this;
                }

                ~GameLogic()
                {
                    Debug.Assert(Instance != null);
                    Instance = null;
                }

                public override void _Ready()
                {
                    //Engine.TimeScale = 0.33f;

                    AddChild(PlayerLookup);
                    AddChild(AIMapLookup);

                    CallDeferred(nameof(SpawnPlayer), new Vector2(0,0));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(64,0));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(-64,0));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(64,256));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(0,-256));

                    CallDeferred(nameof(SpawnAIRider), new Vector2(32,512));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(32,512));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(16,-512));
                    CallDeferred(nameof(SpawnAIRider), new Vector2(48,-512));
                }

                public void SpawnPlayer(Vector2 position)
                {
                    PackedScene riderScene = GD.Load<PackedScene>("res://Scenes/Rider.tscn");

                    KinematicBody2DProxy rider = riderScene.Instance() as KinematicBody2DProxy;

                    SpawnBikeWithRider(rider, position);

                    rider.Model.AddStateEvent<RiderControlState, StateEvent>(new RiderControlEvent.SwitchControlType(PlayerType.KEYBOARD));
                    
                    TeamResourceWrapper teamData = new TeamResourceWrapper(GD.Load("res://Resources/Gangs/Enforcers.tres"));
                    rider.Model.AddStateEvent<TeamState, StateEvent>(new TeamEvent.AssignNewTeam(teamData));

                    PlayerPath = rider.GetPath();
                }

                int count = 0;

                public void SpawnAIRider(Vector2 position)
                {
                    PackedScene riderScene = GD.Load<PackedScene>("res://Scenes/Rider.tscn");

                    KinematicBody2DProxy rider = riderScene.Instance() as KinematicBody2DProxy;

                    SpawnBikeWithRider(rider, position);

                    rider.Model.AddStateEvent<RiderControlState, StateEvent>(new RiderControlEvent.SwitchControlType(PlayerType.AI));
                    if(count%2 == 0)
                    {
                    TeamResourceWrapper teamData = new TeamResourceWrapper(GD.Load("res://Resources/Gangs/Skinners.tres"));
                    rider.Model.AddStateEvent<TeamState, StateEvent>(new TeamEvent.AssignNewTeam(teamData));
                    }
                    else
                    {
                        TeamResourceWrapper teamData = new TeamResourceWrapper(GD.Load("res://Resources/Gangs/Enforcers.tres"));
                        rider.Model.AddStateEvent<TeamState, StateEvent>(new TeamEvent.AssignNewTeam(teamData));
                    }

                    ++count;
                }

                public void SpawnBikeWithRider(KinematicBody2DProxy rider, Vector2 position)
                {
                    PackedScene bikeScene = GD.Load<PackedScene>("res://Scenes/Bike.tscn");

                    KinematicBody2DProxy bike = bikeScene.Instance() as KinematicBody2DProxy;

                    bike.Position = position;

                    Node root = GetTree().CurrentScene;

                    root.AddChild(bike);

                    bike.Model.GetModelPart(out BikeModelPart bikePart);

                    bikePart.AddRiderToRiderPosition(rider);
                    
                    rider.Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.SpawnEvent(bike, true));
                }
    }
}