using Godot;
using ProjectArcadeAssphalt.Singletons;

namespace ProjectArcadeAssphalt.Misc
{
    public class InfiniteScroll : Node2D
    {

        public float _threshold = 0;
        public override void _Process(float delta)
        {

            if (GameLogic.Instance.PlayerLookup.TargetNodeExists)
            {
                KinematicBody2D player = GameLogic.Instance.PlayerLookup.TargetNode;
                // If our second child is below the threshold, we can remove our first child
                float playerPos = player.GlobalPosition.y;
                float childPos = (GetChild(1) as TileMap).Position.y;
                if (playerPos - childPos < -_threshold)
                {
                    TileMap nextMap = GetChild<TileMap>(0);
                    nextMap.GlobalPosition -= new Vector2(0, 1024.0f * (GetChildCount()));
                    MoveChild(nextMap, GetChildCount());
                    //_threshold -= 1024.0f;
                }
            }
        }
    }
}