using AssGameFramework.ProxyNodes;
using Godot;

namespace ProjectArcadeAssphalt.Misc
{
    public class BikeViewNode : Node2D
    {
        [Export]
        public NodePath BodySpritePath { get => BodySpriteLookup.TargetPath; set { BodySpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> BodySpriteLookup { get; set; } = new LookupNode<Sprite>("Body");
        public Sprite BodySprite => BodySpriteLookup.TargetNode;
        
        [Export]
        public NodePath FrontSpritePath { get => FrontSpriteLookup.TargetPath; set { FrontSpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> FrontSpriteLookup { get; set; } = new LookupNode<Sprite>("Front");
        public Sprite FrontSprite => FrontSpriteLookup.TargetNode;

        [Export]
        public NodePath FrontWheelSpritePath { get => FrontWheelSpriteLookup.TargetPath; set { FrontWheelSpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> FrontWheelSpriteLookup { get; set; } = new LookupNode<Sprite>("FrontWheel");
        public Sprite FrontWheelSprite => FrontWheelSpriteLookup.TargetNode;
        
        [Export]
        public NodePath BackWheelSpritePath { get => BackWheelSpriteLookup.TargetPath; set { BackWheelSpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> BackWheelSpriteLookup { get; set; } = new LookupNode<Sprite>("BackWheel");
        public Sprite BackWheelSprite => BackWheelSpriteLookup.TargetNode;

        public override void _Ready()
        {
            AddChild(BodySpriteLookup);
            AddChild(FrontSpriteLookup);
            AddChild(FrontWheelSpriteLookup);
            AddChild(BackWheelSpriteLookup);
        }
    }
}