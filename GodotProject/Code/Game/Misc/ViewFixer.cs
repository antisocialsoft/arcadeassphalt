using Godot;

namespace ProjectArcadeAssphalt
{
    public class ViewFixer : Sprite
    {
        public override void _Process(float delta)
        {
            Position = new Vector2(-0.5f, 0.0f);
            Vector2 globalPos = GlobalPosition;
            Position = new Vector2(-globalPos.x % 1.0f, -globalPos.y % 1.0f);
        }
    }
}