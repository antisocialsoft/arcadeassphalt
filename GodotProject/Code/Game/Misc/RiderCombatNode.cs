using AssGameFramework.ProxyNodes;
using Godot;

namespace ProjectArcadeAssphalt.Misc
{
    public class RiderCombatNode : Node2DProxy
    {
        [Export]
        public NodePath LeftAreaPath { get => LeftAreaLookup.TargetPath; set { LeftAreaLookup.TargetPath = value; } }
        private LookupNode<Area2D> LeftAreaLookup { get; set; } = new LookupNode<Area2D>("LeftCombatArea");
        public Area2D LeftArea => LeftAreaLookup.TargetNode;
        
        [Export]
        public NodePath RightAreaPath { get => RightAreaLookup.TargetPath; set { RightAreaLookup.TargetPath = value; } }
        private LookupNode<Area2D> RightAreaLookup { get; set; } = new LookupNode<Area2D>("RightCombatArea");
        public Area2D RightArea => RightAreaLookup.TargetNode;

        public delegate void AnimationAttackDelegate(AttackDirection attackDir);
        public event AnimationAttackDelegate AnimationAttackEvent;

        public void OnAttackTriggered(AttackDirection attackDir)
        {
            AnimationAttackEvent?.Invoke(attackDir);
        }

        public override void _Ready()
        {
            base._Ready();
            AddChild(LeftAreaLookup);
            AddChild(RightAreaLookup);
        }
    }
}