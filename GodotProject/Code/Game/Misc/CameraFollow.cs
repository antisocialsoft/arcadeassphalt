using System;
using AssGameFramework.Helpers;
using AssGameFramework.DataModel;
using Godot;
using AssGameFramework.ProxyNodes;
using ProjectArcadeAssphalt.Singletons;

namespace ProjectArcadeAssphalt
{
    public class CameraFollow : Camera2D
    {
        [Export]
        public NodePath TargetPath { get => TargetLookup.TargetPath; set { TargetLookup.TargetPath = value; } }
        public LookupNode<Node2D> TargetLookup { get; set; } = new LookupNode<Node2D>("/Node2D/Bike/RiderPos/Rider");

        protected bool _hasTarget = false;
        protected Vector2 _lastPos = new Vector2(0, 0);
        public override void _Ready()
        {
            AddChild(TargetLookup);
        }

        public override void _PhysicsProcess(float delta)
        {
            Node2D target = null;
            if (TargetLookup.TargetNodeExists)
            {
                target = TargetLookup.TargetNode;
            }
            else if (GameLogic.Instance.PlayerLookup.TargetNodeExists)
            {
                target = GameLogic.Instance.PlayerLookup.TargetNode;
                TargetPath = target.GetPath();
            }

            if (target != null)
            {
                if (!_hasTarget)
                {
                    _lastPos = TargetLookup.TargetNode.GlobalPosition;
                    _hasTarget = true;
                }
                Vector2 targetPos = TargetLookup.TargetNode.GlobalPosition;
                Vector2 speed = targetPos - _lastPos;

                float verticalSpeed = Mathf.Abs(speed.y);

                float verticalSize = GetViewportRect().Size.y;

                float targetOffset = (verticalSpeed * 0.085f * verticalSize) - (verticalSize * 0.5f);

                targetOffset = Mathf.Round(Mathf.Min(Mathf.Max(0, targetOffset), verticalSize * 0.40f));

                GlobalPosition = targetPos - new Vector2(0, targetOffset);

                _lastPos = targetPos;

            }
        }
    }
}