using AssGameFramework.ProxyNodes;
using Godot;

namespace ProjectArcadeAssphalt.Misc
{
    public class RiderViewNode : Node2D
    {
        [Export]
        public NodePath AnimationPlayerPath { get => AnimationPlayerLookup.TargetPath; set { AnimationPlayerLookup.TargetPath = value; } }
        private LookupNode<AnimationPlayer> AnimationPlayerLookup { get; set; } = new LookupNode<AnimationPlayer>("AnimationPlayer");
        public AnimationPlayer AnimationPlayer => AnimationPlayerLookup.TargetNode;
                [Export]
        public NodePath OverlaySpritePath { get => OverlaySpriteLookup.TargetPath; set { OverlaySpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> OverlaySpriteLookup { get; set; } = new LookupNode<Sprite>("Overlay");
        public Sprite OverlaySprite => OverlaySpriteLookup.TargetNode;
        [Export]
        public NodePath LegsSpritePath { get => LegsSpriteLookup.TargetPath; set { LegsSpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> LegsSpriteLookup { get; set; } = new LookupNode<Sprite>("Legs");
        public Sprite LegsSprite => LegsSpriteLookup.TargetNode;
        [Export]
        public NodePath BodySpritePath { get => BodySpriteLookup.TargetPath; set { BodySpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> BodySpriteLookup { get; set; } = new LookupNode<Sprite>("Body");
        public Sprite BodySprite => BodySpriteLookup.TargetNode;
        
        [Export]
        public NodePath HeadSpritePath { get => HeadSpriteLookup.TargetPath; set { HeadSpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> HeadSpriteLookup { get; set; } = new LookupNode<Sprite>("Head");
        public Sprite HeadSprite => HeadSpriteLookup.TargetNode;

        [Export]
        public NodePath WeaponSpritePath { get => WeaponSpriteLookup.TargetPath; set { WeaponSpriteLookup.TargetPath = value; } }
        private LookupNode<Sprite> WeaponSpriteLookup { get; set; } = new LookupNode<Sprite>("Weapon");
        public Sprite WeaponSprite => WeaponSpriteLookup.TargetNode;
        public delegate void AnimationAttackDelegate(AttackDirection attackDir);
        public event AnimationAttackDelegate AnimationAttackEvent;

        public void OnAttackTriggered(AttackDirection attackDir)
        {
            AnimationAttackEvent?.Invoke(attackDir);
        }

        public delegate void AttackFinishedDelegate(AttackDirection attackDir);
        public event AttackFinishedDelegate AttackFinishedEvent;

        public void OnAttackFinished(AttackDirection attackDir)
        {
            AttackFinishedEvent?.Invoke(attackDir);
        }

        public override void _Ready()
        {
            AddChild(AnimationPlayerLookup);
            AddChild(BodySpriteLookup);
            AddChild(HeadSpriteLookup);
            AddChild(WeaponSpriteLookup);
            AddChild(LegsSpriteLookup);
            AddChild(OverlaySpriteLookup);
        }
    }
}