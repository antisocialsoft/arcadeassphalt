using Godot;
using ProjectArcadeAssphalt.GameConstants;

namespace ProjectArcadeAssphalt.Portraits
{
    [Tool]
    public class PortraitAttachmentPoint : Control
    {
        [PortraitFlagAttribute]
        public uint AttachmentType {get;set;} = (uint)PortraitConstant.Background;
    }
}