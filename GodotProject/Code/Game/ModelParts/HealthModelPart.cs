using System;
using AssGameFramework.DataModel;
using Godot;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class HealthModelPart : StateMachineModelPart<HealthState, StateEvent>
    {
        [Export]
        public float MaxHealth {get;set;} = 100.0f; // TODO: Resource

        public float CurrentHealth {get;set;} = 0.0f;

        public delegate void HealthZeroDelegate();
        public event HealthZeroDelegate HealthZeroEvent;

        protected override void OnAttachedToModel()
        {
            Model.AddPart<StateMachineModelPart<HealthState, StateEvent>>(this, true);

            StateMachine = new HealthStateMachineModel().CreateInstance(Model);
        }

        internal void InvokeHealthZeroEvent()
        {
            HealthZeroEvent?.Invoke();
        }
    }
}