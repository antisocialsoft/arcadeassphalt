using Godot;
using ProjectArcadeAssphalt.AI;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class BikeAffectorModelPart : MapAffectorModelPart
    {
        public override HeatType AffectorType {get => HeatType.OBSTACLE; }

        public override Rect2 AreaOfInfluence
        {
            get
            {
                Rect2 local = LocalAreaOfInfluence;
                Vector2 heading = HeadingVector;
                float rotation = HeadingDegrees;

                Vector2 tl = (local.Size * new Vector2(0.5f, 0.5f));
                Vector2 tr = (local.Size * new Vector2(-0.5f, 0.5f));

                tl = tl.Rotated(rotation);
                tr = tr.Rotated(rotation);

                tl.x = Mathf.Max(Mathf.Abs(tl.x), Mathf.Abs(tr.x));
                tl.y = Mathf.Max(Mathf.Abs(tl.y), Mathf.Abs(tr.y));

                Vector2 br = -tl;

                return new Rect2(local.Position.Rotated(rotation) + (Owner as Node2D).GlobalPosition, tl-br);

            }
        }

        public Vector2 HeadingVector
        {
            get
            {
                Model.GetModelPart(out MovementModelPart movementPart);
                return movementPart.Velocity;
            }
        }

        public float HeadingDegrees
        {
            get
            {
                float angle = Vector2.Up.AngleTo(HeadingVector);;
                if(angle < 0)
                {
                    angle += Mathf.Pi*2.0f;
                }

                return angle;
            }
        }

        protected Rect2 _localArea;
        [Export]
        public Rect2 LocalAreaOfInfluence 
        {
            get
            {
                float speed = HeadingVector.y;
                return _localArea.GrowIndividual(0, -speed*0.10f, 0, -speed*0.05f);
            }
            set
            {
                _localArea = value;
            }}

        [Export]
        public Curve ValueCurve {get;set;} = new Curve();
        public override uint CollisionFlags => uint.MaxValue;

        public override float CalculateHeatValueForPosition(Vector2 localPos)
        {           
            Rect2 area = AreaOfInfluence;

            localPos = localPos.Rotated(-HeadingDegrees);

            Vector2 bounds = new Vector2(localPos.x/(area.Size.x*0.5f),  localPos.y/(area.Size.y*0.5f));
            float distance = bounds.Length();

            if(distance > 1.0f)
            {
                distance = 1.0f;
            }

            float value = ValueCurve.Interpolate(distance);
            return value;
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();
            Model.AddPart<MapAffectorModelPart>(this, true);
        }
    }
}