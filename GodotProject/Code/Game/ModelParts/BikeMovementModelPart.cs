using System.Diagnostics;
using Godot;
using ProjectArcadeAssphalt.Resources;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class BikeMovementModelPart : MovementModelPart
    {
        [Export]
        public BikeMovementDataResource BikeMovementData {get; set;} = new BikeMovementDataResource();

        public override MovementDataResource MovementData 
        { 
            get => BikeMovementData;
             set { Debug.Assert(value is BikeMovementDataResource); BikeMovementData = value as BikeMovementDataResource; }
        }
        
        public override float TurnAcceleration {get => BikeMovementData.CalculateTurnAcceleration(Velocity.y); }

        protected override void OnAttachedToModel()
        {
            Model.AddPart<MovementModelPart>(this, true);
        }
    }
}