using System;
using System.Diagnostics;
using AssGameFramework.DataModel;
using Godot;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.Resources;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class MovementModelPart : ModelPart
    {
        [Export]
        public virtual MovementDataResource MovementData { get; set; } = new MovementDataResource();
        public float XDirection { get; set; } = 0.0f;

        public float YDirection { get; set; } = 0.0f;

        public KinematicBody2D Body { get => Model.Owner as KinematicBody2D; }

        public Vector2 Velocity { get; set; } = new Vector2(0, 0);
        public bool MovementEnabled { get; set; } = false;

        public bool InputEnabled {get; set; } = true;
        public virtual float MinForwardSpeed => MovementData.MinForwardSpeed;

        public virtual float MinTurnSpeed => MovementData.MinTurnSpeed;

        public virtual float MaxForwardSpeed => MovementData.MaxForwardSpeed;

        public virtual float MaxTurnSpeed => MovementData.MaxTurnSpeed;

        public virtual float ForwardAcceleration => MovementData.ForwardAcceleration;

        public virtual float BackwardsAcceleration => MovementData.BackwardsAcceleration;

        public virtual float TurnAcceleration => MovementData.TurnAcceleration;

        public virtual float ForwardDamping => MovementData.ForwardDamping;

        public virtual float TurnDamping => MovementData.TurnDamping;
    }
}