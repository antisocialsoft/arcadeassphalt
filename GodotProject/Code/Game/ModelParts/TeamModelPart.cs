using AssGameFramework.DataModel;
using Godot;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Resources;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class TeamModelPart : StateMachineModelPart<TeamState, StateEvent>
    {
        [Export]
        public Resource TeamDataResource 
        {
            get { return TeamData?.TeamResourceObj; }
            set { TeamData = new TeamResourceWrapper(value); }
        }

        public TeamResourceWrapper TeamData {get;set;} = null;

        protected override void OnAttachedToModel()
        {
            Model.AddPart<StateMachineModelPart<TeamState, StateEvent>>(this, true);

            StateMachine = new TeamStateMachineModel().CreateInstance(Model);
        }
    }
}