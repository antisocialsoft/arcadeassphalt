﻿using AssGameFramework.Helpers;
using AssGameFramework.DataModel;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using ProjectArcadeAssphalt.Misc;

namespace ProjectArcadeAssphalt.ModelParts
{
    class BikeModelPart : StateMachineModelPart<BikeState, StateEvent>
    {
        public KinematicBody2D BikeBody { get => Model.Owner as KinematicBody2D; }

        [Export]
        public NodePath RiderPointPath { get => RiderPointLookup.TargetPath; set { RiderPointLookup.TargetPath = value; } }
        public LookupNode<Node2D> RiderPointLookup { get; set; } = new LookupNode<Node2D>("../../RiderPos");

        [Export]
        public NodePath ViewPath { get => ViewLookup.TargetPath; set { ViewLookup.TargetPath = value; } }
        public LookupNode<BikeViewNode> ViewLookup { get; set; } = new LookupNode<BikeViewNode>("../../View");

        public Model RiderModel 
        {
            get
            {
                if(RiderPointLookup.TargetNodeExists && RiderPointLookup.TargetNode.GetChildCount() > 0)
                {
                    Debug.Assert(RiderPointLookup.TargetNode.GetChild(0) is IModelProxy);
                    IModelProxy proxy = RiderPointLookup.TargetNode.GetChild(0) as IModelProxy;
                    return proxy.Model;
                }

                return null;
            }
        }

        public override string _GetConfigurationWarning()
        {
            string error = base._GetConfigurationWarning();
            if (error.Length > 0)
                return error;

            if (!(Model.Owner is KinematicBody2D))
            {
                error += "\nModel's owner is not a KinematicBody2D.";
            }

            return error;
        }

        public override void _Ready()
        {
            AddChild(RiderPointLookup);
            AddChild(ViewLookup);
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();

            StateMachine = new BikeStateMachineModel().CreateInstance(Model);
            Model.AddPart<StateMachineModelPart<BikeState, StateEvent>>(this, true);

            BikeMovementComponent movementComp = new BikeMovementComponent();
            AddComponent(movementComp);
        }

        public void AddRiderToRiderPosition(KinematicBody2DProxy rider)
        {
            Debug.Assert(RiderPointLookup.TargetNodeExists);
            if(rider.IsInsideTree())
            {
                rider.GetParent().RemoveChild(rider);
            }

            RiderPointLookup.TargetNode.AddChild(rider);
        }

        
        internal void RemoveRiderFromRiderPosition()
        {
            Debug.Assert(RiderPointLookup.TargetNodeExists);
            if(RiderModel.IsInsideTree())
            {
                KinematicBody2DProxy rider = RiderModel.Owner as KinematicBody2DProxy;
                Vector2 position = rider.GlobalPosition;
                rider.GetParent().RemoveChild(rider);
                Model.Owner.GetParent().AddChild(rider);
                rider.GlobalPosition = position;
            }
        }
    }
}
