using System;
using System.Diagnostics;
using AssGameFramework.DataModel;
using Godot;
using ProjectArcadeAssphalt.AI;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Singletons;

namespace ProjectArcadeAssphalt.ModelParts
{
    public abstract class MapAffectorModelPart : ModelPart
    {
        public abstract uint CollisionFlags {get;}

        public abstract HeatType AffectorType {get;}

        public abstract Rect2 AreaOfInfluence {get;}

        public override void _Ready()
        {
            //Engine.TimeScale = 0.1f;
            if(MapAffectorModelRegistrar.Instance != null)
            {
                MapAffectorModelRegistrar.Instance.RegisterMapAffector(this);
            }
        }

        public abstract float CalculateHeatValueForPosition(Vector2 localPos);
    }
}