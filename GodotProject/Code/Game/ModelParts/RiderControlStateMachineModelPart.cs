using AssGameFramework.DataModel;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class RiderControlStateMachineModelPart : StateMachineModelPart<RiderControlState, StateEvent>
    {
        private AbstractInputComponent _controlComponent = null;
        public AbstractInputComponent ControlComponent
        {
            get => _controlComponent;
            internal set
            {
                if (_controlComponent != null)
                {
                    RemoveComponent(_controlComponent);
                }
                _controlComponent = value;
                if (_controlComponent != null)
                {
                    AddComponent(value);
                }
            }
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();
            Model.AddPart<StateMachineModelPart<RiderControlState, StateEvent>>(this, true);

            StateMachine = new RiderControlStateMachineModel().CreateInstance(Model);
        }
    }
}