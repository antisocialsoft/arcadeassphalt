using System;
using System.Collections.Generic;
using System.Diagnostics;
using AssGameFramework.DataModel;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.AI;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.Resources;
using ProjectArcadeAssphalt.Singletons;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class AIStateMachineModelPart : StateMachineModelPart<AIRiderState, StateEvent>
    {
        #region Timers
        [Export]
        float MovementReactionTime { get; set; } = 0.2f;
        [Export]
        float TargetReactionTime { get; set; } = 1.5f;
        [Export]
        float AttackReactionTime { get; set; } = 0.1f;
        public Timer MovementTimer { get; set; } = new Timer();
        public Timer TargetTimer { get; set; } = new Timer();
        public Timer AttackTimer { get; set; } = new Timer();
        #endregion Timers
        class MovementResults
        {
            public int BestLane { get; set; } = 0;
            public int DistanceFromEdge { get; set; } = 0;
            public int DistanceToObstacle { get; set; } = int.MaxValue;
            public float BestScore { get; set; } = float.MaxValue;
        }

        [Export]
        public AIDataResource AIData { get; set; } = new AIDataResource();
        public Vector2 TargetPos { get; private set; } = Vector2.Zero;

        public float TargetSpeed { get; private set; } = 0;

        public KinematicBody2DProxy Target { get; private set; } = null;

        public HashSet<MapAffectorModelPart> PotentialTargets { get; } = new HashSet<MapAffectorModelPart>();

        private TeamModelPart TeamPart { get => Model.GetModelPart<TeamModelPart>(); }

        private RiderModelPart RiderPart { get => Model.GetModelPart<RiderModelPart>(); }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();
            StateMachine = new AIRiderStateMachineModel().CreateInstance(Model);

            Model.AddPart<StateMachineModelPart<AIRiderState, StateEvent>>(this, true);

            MovementTimer.WaitTime = MovementReactionTime;
            MovementTimer.OneShot = false;
            MovementTimer.Connect("timeout", this, nameof(OnMovementTimeout));
            this.AddChild(MovementTimer);
            TargetTimer.WaitTime = TargetReactionTime;
            TargetTimer.OneShot = false;
            TargetTimer.Connect("timeout", this, nameof(OnTargetTimeout));
            this.AddChild(TargetTimer);
            AttackTimer.WaitTime = AttackReactionTime;
            AttackTimer.OneShot = false;
            AttackTimer.Connect("timeout", this, nameof(OnAttackTimeout));
            this.AddChild(AttackTimer);
        }

        internal override void ModelReady()
        {
            TargetPos = RiderPart.Body.GlobalPosition;
        }

        private void OnAttackTimeout()
        {
            AddStateEvent(new AIRiderEvent.RecalculateAttackEvent());
        }

        private void OnTargetTimeout()
        {
            AddStateEvent(new AIRiderEvent.RecalculateTargetEvent());
        }

        private void OnMovementTimeout()
        {
            AddStateEvent(new AIRiderEvent.RecalculateMovementEvent());
        }

        private float CalculateAvoidanceScore(AIMap aiMap, int xPos, int yPos, int manDist = 1)
        {
            AIMapNode mapNode = aiMap[xPos, yPos];

            float obstacleScore = mapNode.GetHeat(HeatType.OBSTACLE, uint.MaxValue,
            RiderPart.BikeBody.Model.GetModelPart<BikeAffectorModelPart>())
            * Mathf.Pow(AIData.ObstacleDropOff, Mathf.Abs(manDist));

            float dangerScore = mapNode.GetHeat(HeatType.DANGER, (uint)~TeamPart.TeamData.Allies)
                                    * Mathf.Pow(AIData.DangerDropOff, Mathf.Abs(manDist));


            return (obstacleScore * AIData.ObstaclePriority)
             + (dangerScore * AIData.DangerPriority);
        }

        internal void CalculateAttackAction()
        {
            if (Target != null)
            {
                RiderPart.CombatLookup.TargetNode.Model.GetModelPart(out WeaponModelPart weaponPart);
                Vector2 boundingBox = Vector2.Zero;

                Shape2D attackShape = weaponPart.Weapon.AttackShape.Duplicate() as Shape2D;

                switch (attackShape)
                {
                    case CapsuleShape2D capsule:
                        {
                            // Because the radius affects both ends, we only want to scale it by half
                            capsule.Radius += AIData.AttackInaccuracy * capsule.Radius * 0.5f;
                            capsule.Height *= AIData.AttackInaccuracy;
                        }
                        break;
                    default:
                        Debug.Fail("Unsupported attack shape.");
                        break;
                }

                TestAttackArea(attackShape, weaponPart.LeftAttackArea.GlobalTransform, AttackDirection.LEFT);
                TestAttackArea(attackShape, weaponPart.RightAttackArea.GlobalTransform, AttackDirection.RIGHT);
            }
        }

        private void TestAttackArea(Shape2D attackShape, Transform2D transform, AttackDirection attackDir)
        {
            Vector2 bodyPos = RiderPart.Body.GlobalPosition;
            Vector2 closestPoint = Vector2.Inf;
            foreach (int ownerID in Target.GetShapeOwners())
            {
                Transform2D ownerTransform = Target.ShapeOwnerGetTransform((uint)ownerID) * Target.GlobalTransform;
                for (int i = 0; i < Target.ShapeOwnerGetShapeCount((uint)ownerID); ++i)
                {
                    Shape2D targetShape = Target.ShapeOwnerGetShape((uint)ownerID, i);

                    Godot.Collections.Array contacts = attackShape.CollideAndGetContacts(transform, targetShape, ownerTransform);

                    foreach (Vector2 contact in contacts)
                    {
                        Vector2 dif = bodyPos - contact;
                        if (Mathf.Abs(dif.x) < closestPoint.x)
                        {
                            closestPoint.x = (contact - transform.origin).x;
                        }

                        if (Mathf.Abs(dif.y) < closestPoint.y)
                        {
                            closestPoint.y = (contact - transform.origin).y;
                        }
                    }
                }
            }

            float attackChance = 0;
            if (closestPoint.x <= 0 && closestPoint.y <= 0)
            {
                attackChance = AIData.MaxAttackChance;
            }
            else
            {
                Vector2 areaOffset = transform.origin - bodyPos;
                Vector2 chanceVec = areaOffset / (closestPoint + areaOffset);
                attackChance = (chanceVec.x + chanceVec.y) / 2.0f;
            }

            if (attackChance >= AIData.MinViableAttackChance)
            {
                Random random = new Random();
                float roll = (float)random.NextDouble();

                if (roll <= attackChance)
                {
                    Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.HoldAttack(attackDir));
                    Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.ReleaseAttack());
                }
                else
                {
                    Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.HoldAttack(attackDir));
                }
            }

        }

        private float CalculateAttractionScore(AIMap aiMap, int xPos, int yPos, int manDist = 1)
        {
            float targetScore = 0.0f;

            if (Target != null)
            {
                // Target score is inverse, closer we are, the better the position
                Vector2 targetPos = Target.GlobalPosition;

                aiMap.GetNormalisedPositionAtGlobalPosition(targetPos, out int targetX, out int targetY);

                targetScore = Mathf.Abs(xPos - targetX)
                * Mathf.Pow(AIData.TargetDropOff, Mathf.Abs(manDist));
            }

            return targetScore * AIData.TargetPriority;
        }

        internal void CalculateTarget()
        {
            if (GameLogic.Instance.AIMapLookup.TargetNodeExists)
            {
                AIMap aiMap = GameLogic.Instance.AIMapLookup.TargetNode;
                Vector2 pos = RiderPart.Body.GlobalPosition;

                float bestScore = float.MinValue;
                KinematicBody2DProxy currentTarget = Target;
                KinematicBody2DProxy bestTarget = null;
                Target = null;

                foreach (MapAffectorModelPart affector in PotentialTargets)
                {
                    KinematicBody2DProxy target = (affector as ModelPart).Model.Owner as KinematicBody2DProxy;
                    Vector2 targetPos = target.GlobalPosition;

                    // Adjust score based on locality
                    float score = -Math.Abs(pos.y - targetPos.y);
                    score += -Math.Abs(pos.x - targetPos.x);

                    // Give a bonus to the current target
                    if (target == currentTarget)
                    {
                        score *= 1.1f;
                    }

                    if (bestScore < score)
                    {
                        bestScore = score;
                        bestTarget = target;
                    }
                }

                Target = bestTarget;
            }
        }

        public void CalculateMovementTarget()
        {
            if (GameLogic.Instance.AIMapLookup.TargetNodeExists)
            {
                // Clear the potential targets as we're about to repopulate this list anyway
                PotentialTargets.Clear();

                AIMap aiMap = GameLogic.Instance.AIMapLookup.TargetNode;

                aiMap.GetNormalisedPositionAtGlobalPosition(RiderPart.Body.GlobalPosition, out int xPos, out int yPos);

                if (xPos < 0 || yPos < 0)
                {
                    return;
                }

                MovementResults bestResults = new MovementResults();
                CalculateBestLaneScore(aiMap, xPos, yPos, AIData.VisibilityX, AIData.VisibilityY, ref bestResults);

                TargetSpeed = CalculateTargetSpeed(bestResults.DistanceToObstacle);

                TargetPos = aiMap.GetGlobalPositionAtNormalisedPosition((int)bestResults.BestLane, 0);
            }

        }

        private float CalculateTargetSpeed(int distanceToObstacle)
        {
            float targetSpeed = 0.0f;
            if (Target != null && distanceToObstacle > AIData.VisibilityY)
            {
                // If we have a target, we want to try and match speed with them when we're aligned
                Target.Model.GetModelPart(out MovementModelPart movementPart);

                float distanceDif = Target.GlobalPosition.y - RiderPart.Body.GlobalPosition.y;
                if (distanceDif < -16.0f)
                {
                    // Target is ahead of us
                    targetSpeed = movementPart.Velocity.y * (distanceDif / -16.0f);
                }
                else if (distanceDif > 16.0f)
                {
                    // Target is behind us
                    targetSpeed = movementPart.Velocity.y / (distanceDif / -16.0f);
                }
                else
                {
                    // We're already in line with the target
                    targetSpeed = movementPart.Velocity.y;
                }
            }
            else
            {
                // Either we have no target, or we're about to crash, so adjust our speed to avoid crashing
                float speedAdjustment = distanceToObstacle / AIData.VisibilityY;

                Model.GetModelPart(out MovementModelPart movementPart);
                if (speedAdjustment > 1.0f)
                {
                    targetSpeed = Mathf.Max(movementPart.Velocity.y * AIData.IdleSpeedGain, -AIData.TargetIdleSpeed);
                }
                else
                {
                    targetSpeed = movementPart.Velocity.y * speedAdjustment;
                }
            }

            return targetSpeed;
        }

        private void CalculateBestLaneScore(AIMap aiMap, int xPos, int yPos, int xDist, int yDist, ref MovementResults results)
        {
            int obstacleDist = int.MaxValue;

            float runningScore = ProcessLane(aiMap, xPos, yPos, yDist, ref obstacleDist, ref results);

            // Left
            MovementResults leftResults = new MovementResults();
            runningScore += CalculateLaneScore(aiMap, xPos - 1, yPos, -xDist, yDist, runningScore, ref leftResults) * AIData.NeighbourMultiplier;
            // Right
            MovementResults rightResults = new MovementResults();
            runningScore += CalculateLaneScore(aiMap, xPos + 1, yPos, xDist, yDist, runningScore, ref rightResults) * AIData.NeighbourMultiplier;

            // Calculate the results based on the best score between left and right
            if (leftResults.BestScore < results.BestScore * AIData.HeuristicChangeMargin)
            {
                results = leftResults;
            }
            if (rightResults.BestScore < results.BestScore * AIData.HeuristicChangeMargin)
            {
                results = rightResults;
            }

            if (runningScore <= results.BestScore)
            {
                results.BestScore = runningScore;
                results.BestLane = xPos;
                results.DistanceFromEdge = xDist;
                results.DistanceToObstacle = obstacleDist;
            }
            // If the closest obstacle is in front of us, we want that to factor into our speed
            else if (results.DistanceToObstacle > obstacleDist)
            {
                results.DistanceToObstacle = obstacleDist;
            }
        }

        private float ProcessLane(AIMap aiMap, int xPos, int yPos, int yDist, ref int obstacleDist, ref MovementResults results)
        {
            float runningScore = 0.0f;
            for (int y = 0; y < yDist; ++y)
            {
                if (aiMap.HasCell(xPos, yPos + y))
                {
                    float avoidanceScore = CalculateAvoidanceScore(aiMap, xPos, yPos + y, y);

                    runningScore += avoidanceScore;

                    float attractionScore = CalculateAttractionScore(aiMap, xPos, yPos + y, y);

                    runningScore += attractionScore;

                    if (avoidanceScore >= AIData.AvoidanceThreshold && (avoidanceScore - attractionScore) > 0 && results.DistanceToObstacle > y)
                    {
                        obstacleDist = y;
                    }

                    CheckForTarget(aiMap, xPos, yPos + y);

                    aiMap[xPos, yPos + y]._label.Text = avoidanceScore.ToString("n2") + "\n" + attractionScore.ToString("n2");
                }
                else
                {
                    // If there's no tile, assume it's bad and assign a bad score
                    runningScore += y;
                }
            }

            return runningScore;
        }

        private void CheckForTarget(AIMap aiMap, int xPos, int yPos)
        {
            AIMapNode aiNode = aiMap[xPos, yPos];
            aiNode.PopulateListWithAffectors(HeatType.TARGET, AIData.TargetThreshold, (uint)~TeamPart.TeamData.Allies, PotentialTargets);
        }

        private float CalculateLaneScore(AIMap aiMap, int xPos, int yPos, int xDist, int yDist, float neighbourScore, ref MovementResults results)
        {
            if (xDist != 0 && aiMap.HasCell(xPos, yPos))
            {
                int obstacleDist = int.MaxValue;

                float runningScore = ProcessLane(aiMap, xPos, yPos, yDist, ref obstacleDist, ref results);

                float totalScore = runningScore + (neighbourScore * AIData.NeighbourMultiplier);
                totalScore += CalculateLaneScore(aiMap, xPos + Mathf.Sign(xDist), yPos, xDist - Mathf.Sign(xDist), yDist, runningScore, ref results) * AIData.NeighbourMultiplier;

                if (totalScore < results.BestScore * AIData.HeuristicChangeMargin)
                {
                    results.BestScore = totalScore;
                    results.BestLane = xPos;
                    results.DistanceFromEdge = xDist;
                }

                if (Math.Abs(results.DistanceFromEdge) >= Math.Abs(xDist))
                {
                    if (results.DistanceToObstacle > obstacleDist)
                    {
                        results.DistanceToObstacle = obstacleDist;
                    }
                }

                return runningScore;
            }
            else
            {
                return 1.0f * yDist;
            }
        }
    }
}