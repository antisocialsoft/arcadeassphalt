using System;
using System.Diagnostics;
using Godot;
using ProjectArcadeAssphalt.AI;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class WeaponAffectorModelPart : MapAffectorModelPart
    {
        public override HeatType AffectorType { get => HeatType.DANGER; }

        public override Rect2 AreaOfInfluence
        {
            get
            {
                Rect2 local = LocalAreaOfInfluence;
                return new Rect2(local.Position + (Model.Owner as Node2D).GlobalPosition, local.Size);
            }
        }

        public Rect2 LocalAreaOfInfluence { get; private set; } = new Rect2();

        private Rect2 CalculateLocalAreaOfInfluence(WeaponModelPart weaponPart)
        {
            float upperMost = Mathf.Min(weaponPart.LeftAttackArea.Position.y, weaponPart.RightAttackArea.Position.y);
            float bottomMost = Mathf.Max(weaponPart.LeftAttackArea.Position.y, weaponPart.RightAttackArea.Position.y);
            float leftMost = 0;
            float rightMost = 0;

            switch (weaponPart.Weapon.AttackShape)
            {
                case CapsuleShape2D capsule:
                    {
                        leftMost = weaponPart.LeftAttackArea.Position.x - (capsule.Radius * AreaOversize.x);
                        rightMost = weaponPart.RightAttackArea.Position.x + (capsule.Radius * AreaOversize.x);
                        upperMost -= ((capsule.Height * 0.5f) + capsule.Radius) * AreaOversize.y;
                        bottomMost += ((capsule.Height * 0.5f) + capsule.Radius) * AreaOversize.y;
                    }
                    break;
                default:
                    Debug.Fail("Unsupported attack shape.");
                    break;
            }
            return new Rect2(new Vector2((rightMost + leftMost) * 0.5f, (bottomMost + upperMost) * 0.5f),
                        new Vector2(rightMost - leftMost, bottomMost - upperMost)); ;
        }

        [Export]
        public Curve ValueCurve { get; set; } = new Curve();

        [Export]
        public Vector2 AreaOversize { get; set; } = new Vector2(4.0f, 1.0f);

        private WeaponModelPart WeaponPart => Model.GetModelPart<WeaponModelPart>();
        public override uint CollisionFlags
        {
            get
            {
                return (WeaponPart.CurrentState == WeaponState.DISABLED) ? 0 : (uint)WeaponPart.WeaponOwner?.Model.GetModelPart<TeamModelPart>().TeamData.TeamID;
            }
        }

        public override float CalculateHeatValueForPosition(Vector2 localPos)
        {
            Model.GetModelPart<WeaponModelPart>(out WeaponModelPart weaponPart);
            Vector2 distFromCentre = Vector2.Zero;
            Area2D attackArea = (localPos.x < 0) ? weaponPart.LeftAttackArea : weaponPart.RightAttackArea;
            int dirMod = (localPos.x < 0) ? -1 : 1;
            float distance = 0.0f;
            switch (weaponPart.Weapon.AttackShape)
            {
                case CapsuleShape2D capsule:
                    {
                        Vector2 areaOffset = attackArea.Position - new Vector2(capsule.Radius * dirMod * AreaOversize.x, 0);
                        Vector2 distVect = ((localPos - areaOffset) /
                                    new Vector2(capsule.Radius * 2.0f * AreaOversize.x, ((capsule.Height * 0.5f) + capsule.Radius) * AreaOversize.y));

                        if(Mathf.Sign(distVect.x) != dirMod)
                        {
                            // Just needs to be big enough outside of the normalised distance
                            distance = 100.0f;
                        }
                        else
                        {
                            distance = distVect.Length();
                        }
                    }
                    break;
                default:
                    Debug.Fail("Unsupported attack shape.");
                    break;
            }


            if (distance > 1.0f)
            {
                return 0.0f;
            }
            else
            {
                float value = ValueCurve.Interpolate(distance);
                return value;
            }
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();
            Model.AddPart<MapAffectorModelPart>(this, true);
        }

        internal override void ModelReady()
        {
            base.ModelReady();

            LocalAreaOfInfluence = CalculateLocalAreaOfInfluence(Model.GetModelPart<WeaponModelPart>());
        }
    }
}