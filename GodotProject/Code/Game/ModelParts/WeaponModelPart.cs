using System;
using AssGameFramework.DataModel;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.Misc;
using ProjectArcadeAssphalt.Resources;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class WeaponModelPart : StateMachineModelPart<WeaponState, StateEvent>
    {
        public delegate void WeaponSwitchedDelegate();
        public event WeaponSwitchedDelegate WeaponSwitchedEvent;
        [Export]
        private Godot.Resource WeaponResource {get {return _weapon.WeaponResource;} set {_weapon = new WeaponResourceWrapper(value);}}
        private WeaponResourceWrapper _weapon = null;
        public WeaponResourceWrapper Weapon { get {return _weapon; } set {_weapon = value; OnWeaponChanged();} }
        public AttackAreaComponent LeftAttackComponent { get; protected set; } = new AttackAreaComponent(AttackDirection.LEFT);
        public AttackAreaComponent RightAttackComponent { get; protected set; } = new AttackAreaComponent(AttackDirection.RIGHT);

        [Export]
        public NodePath LeftAttackAreaPath { get => LeftAttackAreaLookup.TargetPath; set { LeftAttackAreaLookup.TargetPath = value; } }
        protected LookupNode<Area2D> LeftAttackAreaLookup {get;} = new LookupNode<Area2D>("../../LeftCombatArea");
        public Area2D LeftAttackArea => LeftAttackAreaLookup.TargetNode;

        [Export]
        public NodePath RightAttackAreaPath { get => RightAttackAreaLookup.TargetPath; set { RightAttackAreaLookup.TargetPath = value; } }
        protected LookupNode<Area2D> RightAttackAreaLookup { get; } = new LookupNode<Area2D>("../../RightCombatArea");
        public Area2D RightAttackArea => RightAttackAreaLookup.TargetNode;

        internal IModelProxy WeaponOwner { get; set; } = null;

        protected override void OnAttachedToModel()
        {
            StateMachine = new WeaponStateMachineModel().CreateInstance(Model);
            Model.AddPart<StateMachineModelPart<WeaponState, StateEvent>>(this, true);

            AddChild(LeftAttackAreaLookup);
            AddChild(RightAttackAreaLookup);

            AddComponent(LeftAttackComponent);
            AddComponent(RightAttackComponent);
        }

        private void OnWeaponChanged()
        {
            LeftAttackArea.Position = Weapon.DamageAreaOffset * new Vector2(-1, 1);
            RightAttackArea.Position = Weapon.DamageAreaOffset;

            LeftAttackArea.GetNode<CollisionShape2D>("CollisionShape2D").Shape = Weapon.AttackShape;
            RightAttackArea.GetNode<CollisionShape2D>("CollisionShape2D").Shape = Weapon.AttackShape;

            WeaponSwitchedEvent?.Invoke();   
        }
    }
}