using AssGameFramework.Helpers;
using AssGameFramework.DataModel;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.Components;
using ProjectArcadeAssphalt.GameConstants;
using ProjectArcadeAssphalt.StateMachines;
using ProjectArcadeAssphalt.Misc;
using System;
using System.Diagnostics;

namespace ProjectArcadeAssphalt.ModelParts
{
    public enum PlayerType
    {
        INVALID,
        KEYBOARD,
        AI
    }
    public class RiderModelPart : StateMachineModelPart<RiderState, StateEvent>
    {
        public KinematicBody2D Body { get => Model.Owner as KinematicBody2D; }

        public KinematicBody2DProxy BikeBody { get; set; } = null;

        [Export]
        public NodePath ViewPath { get => ViewLookup.TargetPath; set { ViewLookup.TargetPath = value; } }
        public LookupNode<RiderViewNode> ViewLookup { get; set; } = new LookupNode<RiderViewNode>("../../View");
        public AttackDirection AttackHeld { get; set; } = AttackDirection.NONE;

        public NodePath CombatPath { get => CombatLookup.TargetPath; set { CombatLookup.TargetPath = value; } }
        public LookupNode<Node2DProxy> CombatLookup { get; set; } = new LookupNode<Node2DProxy>("../../Combat");

        public override void _Ready()
        {
            AddChild(ViewLookup);
            AddChild(CombatLookup);
        }
        protected override void OnAttachedToModel()
        {
            StateMachine = new RiderStateMachineModel().CreateInstance(Model);
            Model.AddPart<StateMachineModelPart<RiderState, StateEvent>>(this, true);

            RiderMovementComponent movementComponent = new RiderMovementComponent();
            AddComponent(movementComponent);
            if (ViewLookup.TargetNodeExists)
            {
                ViewLookup.TargetNode.AnimationAttackEvent += OnAttack;
                ViewLookup.TargetNode.AttackFinishedEvent += OnAttackFinished;
            }
        }

        internal override void ModelReady()
        {
            Debug.Assert(Model.Owner is IModelProxy);
            Model.GetModelPart(out HealthModelPart healthPart);
            Debug.Assert(healthPart != null);

            healthPart.HealthZeroEvent += OnHealthZero;
        }

        private void OnHealthZero()
        {
            AddStateEvent(new HealthEvent.HealthZero());
        }

        public void OnAttack(AttackDirection attackDir)
        {
            CombatLookup.TargetNode.Model.AddStateEvent<WeaponState, StateEvent>(new WeaponEvent.DealDamage(attackDir));
        }

        public void OnAttackFinished(AttackDirection attackDir)
        {
            GD.Print("Attack Finished!");
            Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.AttackFinished(attackDir));
        }

        public void OnFall()
        {
            Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.FallOff());
        }
    }
}