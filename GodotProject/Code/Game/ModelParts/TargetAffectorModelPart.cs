using Godot;
using ProjectArcadeAssphalt.AI;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class TargetAffectorModelPart : MapAffectorModelPart
    {
        public override HeatType AffectorType {get => HeatType.TARGET; }

        public override Rect2 AreaOfInfluence
        {
            get
            {
                Rect2 local = LocalAreaOfInfluence;
                return new Rect2(local.Position + (Model.Owner as Node2D).GlobalPosition, local.Size);
            }
        }

        [Export]
        public Rect2 LocalAreaOfInfluence {get;set;}

        private TeamModelPart TeamPart => Model.GetModelPart<TeamModelPart>();
        public override uint CollisionFlags => (uint)TeamPart.TeamData.TeamID;

        public override float CalculateHeatValueForPosition(Vector2 localPos)
        {
            localPos = localPos.Abs();
            Rect2 area = LocalAreaOfInfluence;

            Vector2 bounds = new Vector2(localPos.x/(area.Size.x*0.5f),  localPos.y/(area.Size.y*0.5f));
            float distance = bounds.Length();

            if(distance < 1.0f)
            {
                return 1.0f;
            }
            else
            {
                return 0.0f;
            }
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();
            Model.AddPart<MapAffectorModelPart>(this, true);
        }
    }
}