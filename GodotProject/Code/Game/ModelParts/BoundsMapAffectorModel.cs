using Godot;
using ProjectArcadeAssphalt.AI;

namespace ProjectArcadeAssphalt.ModelParts
{
    public class BoundsMapAffectorModel : MapAffectorModelPart
    {
        public override HeatType AffectorType { get => HeatType.OBSTACLE;}

[Export]
public Rect2 LocalAreaOfInfluence {get;set;} = new Rect2(Vector2.Zero, new Vector2(640.0f, 1024.0f));
        public override Rect2 AreaOfInfluence 
        {
            get 
            {
                Rect2 area = LocalAreaOfInfluence;
                area.Position += (Model.GetParent() as Node2D).GlobalPosition;

                return area;
            }
        }

        public override uint CollisionFlags => uint.MaxValue;

        public override float CalculateHeatValueForPosition(Vector2 localPos)
        {
                float xPos = Mathf.Abs(localPos.x);
                if(xPos > 32*5)
                {
                    return 10.0f;
                }
                else if(xPos > 32*4)
                {
                    return 1.0f;
                }

            return 0.0f;
        }

        public override void _Ready()
        {
            base._Ready();
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();
            Model.AddPart<MapAffectorModelPart>(this, true);
        }
    }
}