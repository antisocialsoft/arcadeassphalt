using System;
using AssGameFramework.Components;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.AI;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.Singletons;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.Components
{
    public class AIInputComponent : AbstractInputComponent
    {
        protected MovementModelPart MovementPart {get => Model.GetModelPart<MovementModelPart>(); }
        protected AIStateMachineModelPart AIPart {get => Model.GetModelPart<AIStateMachineModelPart>(); }
        protected Vector2 MovementDir  {get; set;} = new Vector2(0,0);
        protected float MovementDirX  {get => MovementDir.x; set {MovementDir = new Vector2(value, MovementDir.y);}}
        protected float MovementDirY  {get => MovementDir.y; set {MovementDir = new Vector2(MovementDir.x, value);}}

        public AIInputComponent()
        {
            Name = "AIInputComponent";
        }

        protected AttackDirection _lastAttackDir = AttackDirection.NONE;

        public override void _PhysicsProcess(float delta)
        {
            if(!GameLogic.Instance.AIMapLookup.TargetNodeExists)
            {
                return;
            }
            AIMap aiMap = GameLogic.Instance.AIMapLookup.TargetNode;

            Vector2 targetPos = AIPart.TargetPos;

            if(Mathf.Abs(targetPos.x - MovementPart.Body.GlobalPosition.x) > aiMap.CellSize.x * 0.35f)
            {
                float dir = (targetPos.x < MovementPart.Body.GlobalPosition.x)? -1 : 1;
                if(dir != MovementDirX)
                {
                    Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.TurnEvent(-MovementDirX+dir, true));
                    MovementDirX = dir;
                }
            }
            else
            {
                if(MovementDirX != 0)
                {
                    Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.TurnEvent(-MovementDirX, false));
                    MovementDirX = 0;
                }
            }

            float speedDif = AIPart.TargetSpeed - MovementPart.Velocity.y;
            if(Mathf.Abs(speedDif) > 5.0f)
            {
                float newDir = Mathf.Sign(speedDif);
                if(MovementDirY != newDir)
                {
                Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.WalkEvent(-MovementDirY+newDir));
                MovementDirY = newDir;
                }
            }
            else if(MovementDirY != 0)
            {
                Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.WalkEvent(-MovementDirY));
                MovementDirY = 0;
            }
        }
    }
}