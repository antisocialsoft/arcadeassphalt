using AssGameFramework.Components;
using Godot;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.Components
{
    public class KeyboardInputComponent : AbstractInputComponent
    {
        protected MovementModelPart InputModel { get => Model.GetModelPart<MovementModelPart>(); }
        
public KeyboardInputComponent()
{
    Name = "KeyboardInputComponent";
}

protected AttackDirection _lastAttackDir = AttackDirection.NONE;
        public override void _Input(Godot.InputEvent @event)
        {
            base._Input(@event);

            Vector2 dir = new Vector2();
            bool pressed = true;

            if(!InputModel.InputEnabled)
            {
                return;
            }

            if (@event.IsActionType())
            {
                if (@event.IsPressed())
                {
                    if (@event.IsActionPressed("ui_left"))
                    {
                        dir.x = -1.0f;
                        _lastAttackDir = AttackDirection.LEFT;
                    }
                    else if (@event.IsActionPressed("ui_right"))
                    {
                        dir.x = 1.0f;
                        _lastAttackDir = AttackDirection.RIGHT;
                    }
                    else if (@event.IsActionPressed("ui_up"))
                    {
                        dir.y = -1.0f;
                    }
                    else if (@event.IsActionPressed("ui_down"))
                    {
                        dir.y = 1.0f;
                    }
                    else if (@event.IsActionPressed("ui_select"))
                    {
                        Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.HoldAttack(_lastAttackDir));   
                    }
                }
                else
                {
                    pressed = false;
                    if (@event.IsActionReleased("ui_left"))
                    {
                        dir.x = 1.0f;
                    }
                    else if (@event.IsActionReleased("ui_right"))
                    {
                        dir.x = -1.0f;
                    }
                    else if (@event.IsActionReleased("ui_up"))
                    {
                        dir.y = 1.0f;
                    }
                    else if (@event.IsActionReleased("ui_down"))
                    {
                        dir.y = -1.0f;
                    }
                    else if(@event.IsActionReleased("ui_select"))
                    {
                        Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.ReleaseAttack());
                    }
                }
            }

            if (dir.x != 0)
            {
                Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.TurnEvent(dir.x, pressed));
            }
            else if(dir.y != 0)
            {
                Model.AddStateEvent<RiderState, StateEvent>(new RiderEvent.WalkEvent(dir.y));
            }
        }
    }
}