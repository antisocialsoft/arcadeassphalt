using System;
using System.Diagnostics;
using AssGameFramework.Components;
using AssGameFramework.ProxyNodes;
using Godot;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.StateMachines;

namespace ProjectArcadeAssphalt.Components
{
    public class AttackAreaComponent : NodeComponent
    {
        public AttackDirection AttackDirection { get; }
        protected WeaponModelPart WeaponModelPart { get => Model.GetModelPart<WeaponModelPart>(); }

        protected Area2D AttackArea
        {
            get
            {
                switch (AttackDirection)
                {
                    case AttackDirection.LEFT:
                        return WeaponModelPart.LeftAttackArea;
                    case AttackDirection.RIGHT:
                        return WeaponModelPart.RightAttackArea;

                }
                Debug.Fail("Invalid attack direction.");
                return null;
            }
        }

        public AttackAreaComponent(AttackDirection attackDir)
        {
            AttackDirection = attackDir;
            Name = "Attack Area Component";
        }

        internal void DealDamage()
        {
            foreach(Node collided in AttackArea.GetOverlappingBodies())
            {
                if(collided is IModelProxy proxy)
                {
                    //TODO: Deal damage
                    GD.Print("Bonk!");
                    proxy.Model.AddStateEvent<HealthState, StateEvent>(new HealthEvent.TakeDamage(200.0f));
                }
            }
        }
    }
}