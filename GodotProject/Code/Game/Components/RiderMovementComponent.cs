using AssGameFramework.Components;
using Godot;
using ProjectArcadeAssphalt.ModelParts;

namespace ProjectArcadeAssphalt.Components
{
    public class RiderMovementComponent : NodeComponent
    {
        protected MovementModelPart MovementModel { get => Model.GetModelPart<MovementModelPart>(); }

        public RiderMovementComponent()
        {
            Name = "RiderMovementComp";
        }
        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);
            if (MovementModel.MovementEnabled)
            {
                KinematicBody2D riderBody = MovementModel.Body;
                Vector2 velocity = MovementModel.Velocity;
                velocity.x -= velocity.x * 2.0f * delta;

                Vector2 movement = new Vector2(MovementModel.XDirection, MovementModel.YDirection);

                 // Adjust velocity with damping
                float damping = velocity.x * MovementModel.TurnDamping * delta;
                if (Mathf.Abs(velocity.x - damping) < MovementModel.MinTurnSpeed)
                {
                    velocity.x = MovementModel.MinTurnSpeed * Mathf.Sign(velocity.x);
                }
                else
                {
                    velocity.x -= damping;
                }

                damping = velocity.y * MovementModel.ForwardDamping * delta;
                if (Mathf.Abs(velocity.y - damping) < MovementModel.MinForwardSpeed)
                {
                    velocity.y = MovementModel.MinForwardSpeed;
                }
                else
                {
                    velocity.y -= damping;
                }

                MovementModel.Velocity = riderBody.MoveAndSlide(velocity);
            }
        }
    }
}