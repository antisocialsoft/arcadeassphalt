﻿using AssGameFramework.Components;
using Godot;
using ProjectArcadeAssphalt.ModelParts;
using ProjectArcadeAssphalt.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectArcadeAssphalt.Components
{
    class BikeMovementComponent : NodeComponent
    {
        protected MovementModelPart MovementModel { get => Model.GetModelPart<MovementModelPart>(); }

        public BikeMovementComponent()
        {
            Name = "BikeMovementComp";
        }
        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);
            if (MovementModel.MovementEnabled)
            {
                KinematicBody2D bikeBody = MovementModel.Body;
                Vector2 velocity = MovementModel.Velocity;

                Vector2 movement = new Vector2(MovementModel.XDirection, MovementModel.YDirection);

                // Adjust movement based on minimum values
                if (movement.x == 0.0f && Mathf.Abs(velocity.x) < MovementModel.MinTurnSpeed)
                {
                    movement.x = Mathf.Sign(velocity.x);
                }

                if (-velocity.y < MovementModel.MinForwardSpeed)
                {
                    movement.y = -1.0f;
                }

                // Adjust velocity with damping
                float damping = velocity.x * MovementModel.TurnDamping * delta;
                if (Mathf.Abs(velocity.x - damping) < MovementModel.MinTurnSpeed)
                {
                    velocity.x = MovementModel.MinTurnSpeed * Mathf.Sign(velocity.x);
                }
                else
                {
                    velocity.x -= damping;
                }

                damping = velocity.y * MovementModel.ForwardDamping * delta;
                if (Mathf.Abs(velocity.y - damping) < MovementModel.MinForwardSpeed)
                {
                    velocity.y = -MovementModel.MinForwardSpeed;
                }
                else
                {
                    velocity.y -= damping;
                }

                velocity.x += movement.x * MovementModel.TurnAcceleration * delta;

                if(movement.y < 0)
                {
                    velocity.y += movement.y * MovementModel.ForwardAcceleration * delta;
                }
                else
                {
                    velocity.y += movement.y * MovementModel.BackwardsAcceleration * delta;
                }
                // Adjust velocity for maximum speeds
                velocity.x = Mathf.Sign(velocity.x) * Mathf.Min(Mathf.Abs(velocity.x), MovementModel.MaxTurnSpeed);
                velocity.y = Mathf.Sign(velocity.y) * Mathf.Min(Mathf.Abs(velocity.y), MovementModel.MaxForwardSpeed);

                MovementModel.Velocity = bikeBody.MoveAndSlide(velocity);

                // This is used to alert any riders that our velocity has changed
                Model.AddStateEvent<BikeState, StateEvent>(new StateEvent.UpdateVelocity(MovementModel.Velocity));
            }
        }
    }
}
